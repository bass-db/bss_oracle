function [Se,W,SDR]=bss_oracle_pinvmask(X,S,L,A,Ja)

% BSS_ORACLE_PINVMASK Oracle estimator for multichannel source
% separation of instantaneous mixtures by time-frequency masking and
% mixing matrix pseudo-inversion using MDCT with sine window.
%
% [Se,W,SDR]=bss_oracle_pinvmask(X,S,L,A)
% [Se,W,SDR]=bss_oracle_pinvmask(X,S,L,A,Ja)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: I x T x J table containing the target signals (source images)
% L: length of the MDCT window in samples (must be a multiple of 4)
% A: I x J real-valued mixing matrix (may be different from the one actually
%     used to generate S)
% Ja: number of active sources per time-frequency point (by default or if Ja=0,
%      the best number is estimated for each time-frequency point)
%
% Outputs:
% Se: I x T x J table containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: L/2 x N x J table of binary coefficients indicating the oracle source
% activity patterns with N=ceil(T/L*2)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 6.2.
%
% See also mdct, imdct.

%%% Errors and warnings %%%
if nargin<4, error('Not enough input arguments.'); end
if nargin<5, Ja=0; end
[I,T]=size(X);
[Is,Ts,J]=size(S);
[Im,Jm]=size(A);
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if I~=Is, error('Mixture and target signals have a different number of channels.'); end
if I~=Im, error('The number of rows in the mixing matrix must be equal to the number of mixture signals.'); end
if J~=Jm, error('The number of columns in the mixing matrix must be equal to the number of sources.'); end
if Ja>I, error('The number of active sources must be smaller than the number of sources.'); end
if any(any(abs(X-sum(S,3)) > eps)), error('The mixture signal must be equal to the sum of the target signals.'); end
N=ceil(T/L*2);

%%% Computing MDCT of input data %%%
XX=zeros(L/2,N,I);
SS=zeros(L/2,N,I,J);
for i=1:I,
    XX(:,:,i)=mdct(X(i,:),L);
    for j=1:J,
        SS(:,:,i,j)=mdct(S(i,:,j),L);
    end
end
XX=reshape(XX,L/2*N,I);

%%% Selecting the best active sources %%%
% Source activity patterns
act=zeros(2^J,J);
for p=1:J,
    act(:,p)=reshape(repmat([0 1],[2^(J-p) 2^(p-1)]),2^J,1);
end
% Distortion for each pattern
disto=zeros(L/2,N,2^J);
for p=1:2^J,
    ind=find(act(p,:));
    nind=length(ind);
    if (nind==Ja) || (Ja==0),
        % Pseudo-inversion of mixing matrix for active sources
        B=pinv(A(:,ind));
        SSe=zeros(L/2,N,I,J);
        for j=1:nind,
            SSe(:,:,:,ind(j))=reshape(XX*B(j,:).'*A(:,ind(j)).',L/2,N,I);
        end
        disto(:,:,p)=sum(sum((SSe-SS).^2,4),3);
    else
        % Infinite distortion for disallowed number of active sources
        disto(:,:,p)=Inf;
    end
end
% Selecting best pattern and deriving oracle target MDCTs
[disto,pat]=min(disto,[],3);
W=zeros(L/2*N,J);
SSe=zeros(L/2*N,J);
for p=1:2^J,
    ind=find(act(p,:));
    n=find(pat==p);
    W(n,ind)=1;
    B=pinv(A(:,ind));
    SSe(n,ind)=XX(n,:)*B.';
end
W=reshape(W,L/2,N,J);
SSe=reshape(SSe,L/2,N,J);

%%% Deriving oracle estimates of the target signals %%%
% Target signals
Se=zeros(I,N*L/2,J);
for j=1:J,
    Se(:,:,j)=A(:,j)*imdct(SSe(:,:,j));
end
% SDR
SDR=sdr(reshape(Se,I*N*L/2,J),reshape(cat(2,S,zeros(I,N*L/2-T,J)),I*N*L/2,J));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;
