\documentclass[11pt,a4paper]{book}
\usepackage[latin1]{inputenc}
\usepackage[english]{babel}
\usepackage{amssymb,amsfonts,amsmath,amscd,url,theorem,ulem,graphicx,tabularx}
\usepackage[a4paper,textwidth=16cm,top=2cm,bottom=4cm,left=2.5cm,right=2.5cm]{geometry}

\newcommand{\mysection}[1]{\begin{center} \framebox[16cm][l]{\texttt{#1}} \end{center}\addcontentsline{toc}{subsection}{\texttt{#1}} }
\newcommand{\mysubsection}[1]{\bigskip\noindent{\bf #1}:\\}
\newcommand{\matlab}[0]{Matlab$^\text{\textregistered}$}
\DeclareMathOperator{\ceil}{ceil}
\DeclareMathOperator{\nextpow2}{nextpow2}

\title{\Huge \bf BSS Oracle Toolbox Version 2.1\\[.5em]User Guide}
\author{\Large  Emmanuel \sc{Vincent}
  \protect\\
  \protect\\
  \Large R\'emi \sc{Gribonval}
  \protect\\
  \protect\\
  \Large Mark D. \sc{Plumbley}\vspace{5cm}}
\date{\Large October 12, 2007\vspace{5cm}}

\begin{document}

\maketitle

\vfill
 \tableofcontents

\vfill

\chapter{Getting started}

\section{Download and install}
\label{sec:download}
Two versions of the BSS Oracle toolbox are available.\\[1em]
The basic version of the toolbox, which includes the main \matlab\footnote{\matlab\ is a registered trademark of The MathWorks, Inc.} programs and this user guide, can be downloaded at\\
\texttt{http://bass-db.gforge.inria.fr/bss\_oracle/bss\_oracle\_basic.zip\\}
After unzipping this file, you should get a directory called \texttt{bss\_oracle\_2.1}. To install, simply add the full path to this directory to your \matlab\ path using the command \texttt{pathtool}.\\[1em]
The full version of the toolbox, which includes additional example data and \matlab\ programs, can be downloaded at\\
\texttt{http://bass-db.gforge.inria.fr/bss\_oracle/bss\_oracle\_full.zip\\}
After unzipping this file, you should get in addition to the main directory called \texttt{bss\_oracle\_2.1} two sub-directories called \texttt{examples} and \texttt{data}. To install, simply add the full paths to the main directory and to the sub-directories to your \matlab\ path.

\section{Software dependencies}
\label{sec:dependencies}
BSS Oracle consists in a set of \matlab functions, and as such needs \matlab\ to run.\\[1em]
Some functions of BSS Oracle (involving MDCT, CP or WP transforms) also depend on the Wavelab toolbox version 802 by D. Donoho, M.R. Duncan, X. Huo and O. Levi, available at\\
\texttt{http://www-stat.stanford.edu/\~{}wavelab/}\\
Follow the provided documentation for install instructions. Note that Wavelab is copyrighted and cannot be redistributed together with BSS Oracle.

\section{Getting help}
\label{sec:help}
Within \matlab, you can get basic help about the toolbox by typing \texttt{help bss\_oracle\_2.1}
\newpage

\section{Citation}
\label{sec:citation}
If you use the BSS Oracle toolbox in a work that you wish to publish, please cite it as:\\[.5em]
E. Vincent, R. Gribonval and M.D. Plumbley. BSS Oracle Toolbox Version 2.1.\\
\texttt{http://bass-db.gforge.inria.fr/bss\_oracle/}

\section{Licenses}
\label{sec:license}
The files contained in the BSS Oracle toolbox are distributed under different licenses. Therefore it is crucial that you understand which license applies to each file before attempting to redistribute or modify some files.\\[1em]
The files contained in the main directory \texttt{bss\_oracle\_2.1} and in the subdirectory \texttt{examples} are distributed under the terms of the GNU General Public License (GPL) version 2. A copy of the GPL is distributed along with the toolbox in the file \texttt{LICENSE.txt}.\\[1em]
The music sound files contained in the subdirectory \texttt{data} are distributed under specific Creative Commons licenses. For more details about the license applying to each file, see the file \texttt{data/LICENSES.txt}.\\[1em]
All other files of the subdirectory \texttt{data} are license free.

\vfill

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\chapter{Content}
\label{cha:user-guide}
The purpose of the BSS Oracle toolbox is to compute the best performance achievable by a class of source separation algorithms in an evaluation framework where target signals are known.\\
It does not provide any blind source separation method.

\section{Reference publications}
\label{sec:article}
The mathematical details underlying the toolbox are described in \cite{Vin07a,Vin07c,Vin06}.

\section{Principle}
\label{sec:principle}
Let us suppose that we observe a mixture signal $\mathbf{x}(t)$ from which we want to extract a set of source signals $\mathbf{y}(t)$. Within a given class of source separation algorithms, the estimated signal $\widehat{\mathbf{y}}(t)$ can always be expressed under the form
\begin{equation}
\widehat{\mathbf{y}}=f(\mathbf{x},\theta)\quad\text{with }\theta\in\Theta,
\end{equation}
where $f$ is a fixed parametric function, $\theta$ a vector of separation parameters and $\Theta$ a set of acceptable parameters. Different algorithms correspond to different ways of estimating $\theta$.

Assuming that the target signal $\mathbf{y}(t)$ is known, the separation performance of a given algorithm can be evaluated using the Euclidean distortion measure
\begin{equation}
d(\widehat{\mathbf{y}},\mathbf{y})=\|\widehat{\mathbf{y}}-\mathbf{y}\|^2.
\end{equation}
The oracle estimator of the target signal is then defined by
\begin{equation}
\widetilde{\mathbf{y}}(\mathbf{y},\mathbf{x},\Theta)=f(\mathbf{x},\widetilde{\theta}(\mathbf{y},\mathbf{x},\Theta)),
\end{equation}
where $\widetilde{\theta}(\mathbf{y},\mathbf{x},\Theta)$ is the set of parameters resulting in the smallest distortion among the set of acceptable parameters $\Theta$:
\begin{equation}
\widetilde{\theta}(\mathbf{y},\mathbf{x},\Theta)=\arg\min_{\theta\in\Theta} d(f(\mathbf{x},\theta),\mathbf{y}).
\end{equation}

\section{Summary of the content}
\label{sec:summary}
The basic version of the toolbox implements oracle source estimators for four classes of algorithms: multichannel time-invariant filtering, single-channel time-frequency masking, multichannel time-frequency masking and best basis masking. In some cases, the exact oracle estimators cannot be computed due to high memory and/or computational time requirements. Thus near-optimal source estimators are implemented instead.

The full version of the toolbox also contains example data and routines that were used to create the figures of the reference publications.

\chapter{Reference manual}
\label{cha:reference-manual}
%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{General notations}
\label{sec:notations}
\bigskip
\begin{tabularx}{\textwidth}{p{2cm}X}
$T$ & length of the signals in samples\\
$I$ & number of channels of the mixture signal\\
$J$ & number of source or target signals\\
$L$ & length of the demixing filters, or MDCT/STFT length\\
$M$ & stepsize between successive STFT windows\\
$K$ & number of mixture signals (for generic oracle bases)\\
\end{tabularx}\\[1em]
\newpage

\section{Oracle source estimators}
\label{sec:oracle}
\bigskip

\mysection{bss\_oracle\_multifilt}
Oracle estimator for source separation by multichannel time-invariant filtering in the time domain.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_oracle\_multifilt(X,S,L)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (e.g. sources or source images)\\
\texttt{L} & length of the demixing filters in samples\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $J\times I\times L$ table containing the coefficients of the oracle demixing filters (delays from $-\frac{L}{2}+1$ to $\frac{L}{2}$)\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 4.2.
\newpage

\mysection{bss\_oracle\_monomask}
Oracle estimator for single-channel source separation by time-frequency masking using MDCT with sine window.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_oracle\_monomask(x,S,L)}\\
\texttt{[Se,W,SDR]=bss\_oracle\_monomask(x,S,L,mreal)}\\
\texttt{[Se,W,SDR]=bss\_oracle\_monomask(x,S,L,mreal,mconst)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times T$ vector containing the single-channel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (e.g. source images)\\
\texttt{L} & length of the MDCT window in samples (must be a multiple of 4)\\
\texttt{mreal} & \texttt{true} for real-valued masking (default), \texttt{false} for binary masking\\
\texttt{mconst} & \texttt{true} when the masks are subject to a unitary sum constraint (default), \texttt{false} otherwise\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $\frac{L}{2}\times N\times J$ table containing the oracle masks with $N=\ceil\left(\frac{2T}{L}\right)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 5.2.
\newpage

\mysection{bss\_oracle\_binmask}
Oracle estimator for multichannel source separation by constrained binary time-frequency masking using MDCT with sine window.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_oracle\_binmask(X,S,L)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $I\times T\times J$ table containing the target signals (source images)\\
\texttt{L} & length of the MDCT window in samples (must be a multiple of 4)\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $I\times T\times J$ table containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $\frac{L}{2}\times N\times J$ table containing the oracle masks with $N=\ceil\left(\frac{2T}{L}\right)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 6.2.
\newpage

\mysection{bss\_oracle\_pinvmask}
Oracle estimator for multichannel source separation of instantaneous mixtures by time-frequency masking and mixing matrix pseudo-inversion using MDCT with sine window.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_oracle\_pinvmask(X,S,L,A)}\\
\texttt{[Se,W,SDR]=bss\_oracle\_pinvmask(X,S,L,A,Ja)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $I\times T\times J$ table containing the target signals (source images)\\
\texttt{L} & length of the MDCT window in samples (must be a multiple of 4)\\
\texttt{A} & $I\times J$ real-valued mixing matrix (may be different from the one actually used to generate $S$)\\
\texttt{Ja} & number of active sources per time-frequency point (by default or if $Ja=0$, the best number is estimated for each time-frequency point)\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $I\times T\times J$ table containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $\frac{L}{2}\times N\times J$ table of binary coefficients indicating the oracle source activity patterns with $N=\ceil\left(\frac{2T}{L}\right)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 6.2.
\newpage

\mysection{bss\_oracle\_bbasis\_monomask}
Oracle estimator for single-channel source separation by time-frequency masking using the best CP/WP basis.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_monomask(x,S,Dmin,Dmax)}\\
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_monomask(x,S,Dmin,Dmax,pcos)}\\
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_monomask(x,S,Dmin,Dmax,pcos,mreal)}\\
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_monomask(x,S,Dmin,Dmax,pcos,mreal,mconst)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times T$ vector containing the single-channel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (e.g. source images)\\
\texttt{Dmin} & minimal packet depth\\
\texttt{Dmax} & maximal packet depth\\
\texttt{pcos} & \texttt{true} for CP basis with sine window (default), \texttt{false} for WP basis with symmlet-8\\
\texttt{mreal} & \texttt{true} for real-valued masking (default), \texttt{false} for binary masking\\
\texttt{mconst} & \texttt{true} when the masks are subject to a unitary sum constraint (default), \texttt{false} otherwise\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{btree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector of binary values representing the tree structure corresponding to the oracle best basis\\
\texttt{W} & $2^N\times (D_\mathrm{max}-D_\mathrm{min}+1)\times J$ table containing the oracle masking coefficients for each scale with $N=\nextpow2(T)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\texttt{stree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector containing the oracle distortion for all basis elements (infinite for disallowed scales)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin06} Section 7.2.
\newpage

\mysection{bss\_oracle\_bbasis\_binmask}
Oracle estimator for multichannel source separation by constrained binary time-frequency masking using the best CP/WP basis.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_binmask(X,S,Dmin,Dmax)}\\
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_binmask(X,S,Dmin,Dmax,pcos)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (source images)\\
\texttt{Dmin} & minimal packet depth\\
\texttt{Dmax} & maximal packet depth\\
\texttt{pcos} & \texttt{true} for CP basis with sine window (default), \texttt{false} for WP basis with symmlet-8\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{btree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector of binary values representing the tree structure corresponding to the oracle best basis\\
\texttt{W} & $2^N\times (D_\mathrm{max}-D_\mathrm{min}+1)\times J$ table containing the oracle masking coefficients for each scale with $N=\nextpow2(T)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\texttt{stree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector containing the oracle distortion for all basis elements (infinite for disallowed scales)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07c}.
\newpage

\mysection{bss\_oracle\_bbasis\_pinvmask}
Oracle estimator for multichannel source separation of instantaneous mixtures by time-frequency masking and mixing matrix pseudo-inversion using the best CP/WP basis.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_pinvmask(X,S,A,Dmin,Dmax)}\\
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_pinvmask(X,S,A,Dmin,Dmax,Ja)}\\
\texttt{[Se,btree,W,SDR,stree]=bss\_oracle\_bbasis\_pinvmask(X,S,A,Dmin,Dmax,Ja,pcos)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (source images)\\
\texttt{A} & $I\times J$ real-valued mixing matrix (may be different from the one actually used to generate $S$)\\
\texttt{Dmin} & minimal packet depth\\
\texttt{Dmax} & maximal packet depth\\
\texttt{Ja} & number of active sources per time-frequency point (by default or if $Ja=0$, the best number is estimated for each time-frequency point)\\
\texttt{pcos} & \texttt{true} for CP basis with sine window (default), \texttt{false} for WP basis with symmlet-8\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{btree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector of binary values representing the tree structure corresponding to the oracle best basis\\
\texttt{W} & $2^N\times (D_\mathrm{max}-D_\mathrm{min}+1)\times J$ table containing the oracle masking coefficients for each scale with $N=\nextpow2(T)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\texttt{stree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector containing the oracle distortion for all basis elements (infinite for disallowed scales)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07c} Section 3.2.
\newpage

\mysection{bss\_oracle\_gbasis\_monomask}
Oracle estimator for single-channel source separation of several mixtures by time-frequency masking using the best generic CP/WP basis.\\[1em]
\mysubsection{Syntax}
\texttt{[Se,btree,SDR,gSDR,stree]=bss\_oracle\_gbasis\_monomask(x,S,Dmin,Dmax)}\\
\texttt{[Se,btree,SDR,gSDR,stree]=bss\_oracle\_gbasis\_monomask(x,S,Dmin,Dmax,pcos)}\\
\texttt{[Se,btree,SDR,gSDR,stree]=bss\_oracle\_gbasis\_monomask(x,S,Dmin,Dmax,pcos,mreal)}\\
\texttt{[Se,btree,SDR,gSDR,stree]=bss\_oracle\_gbasis\_monomask(x,S,Dmin,Dmax,pcos,mreal,mconst)}\\[1em]
\mysubsection{Warnings}
Despite the default setting, this function is mostly relevant for WP bases.\\
Due to huge memory requirements, the optimal masks are not output.\\
The function stores temporary data in a temporary directory defined by the variable \texttt{tmpdir} (default is \texttt{/tmp/}).\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times T\times K$ table containing $K$ single-channel mixture signals\\
\texttt{S} & $J\times T\times K$ table containing $K$ sets of target signals (e.g. source images)\\
\texttt{Dmin} & minimal packet depth\\
\texttt{Dmax} & maximal packet depth\\
\texttt{pcos} & \texttt{true} for CP basis with sine window (default), \texttt{false} for WP basis with symmlet-8\\
\texttt{mreal} & \texttt{true} for real-valued masking (default), \texttt{false} for binary masking\\
\texttt{mconst} & \texttt{true} when the masks are subject to a unitary sum constraint (default), \texttt{false} otherwise\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T\times K$ table containing the oracle estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{btree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector of binary values representing the tree structure corresponding to the oracle best generic basis\\
\texttt{SDR} & $K\times 1$ vector containing the achieved $\mathrm{SDR}$ in deciBels for each mixture (before truncation of the target estimates)\\
\texttt{gSDR} & total $\mathrm{SDR}$ for all mixtures\\
\texttt{stree} & $1\times (2^{D_\mathrm{max}+1}-1)$ vector containing the oracle distortion for all basis elements (infinite for disallowed scales)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin06} Section 7.3.2.
\newpage

\section{Near-optimal source estimators}
\label{sec:nearopt}
\bigskip

\mysection{bss\_nearopt\_multifilt}
Near-optimal demixing matrices for source separation by frequency-domain multichannel time-invariant filtering using STFT with sine window (coefficients derived for each frequency bin separately).\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_nearopt\_multifilt(X,S,L)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_multifilt(X,S,L,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (e.g. sources or source images)\\
\texttt{L} & length of the STFT window in samples (must be a multiple of 4)\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the near-optimal estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $J\times I\times \left(\frac{L}{2}+1\right)$ table containing near-optimal demixing matrices for positive frequencies\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 4.4.
\newpage

\mysection{bss\_nearopt\_monomask}
Near-optimal time-frequency masks for single-channel source separation using STFT with sine window (coefficients derived for each time-frequency point separately).\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_nearopt\_monomask(x,S,L)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_monomask(x,S,L,M)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_monomask(x,S,L,M,mreal)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_monomask(x,S,L,M,mreal,mconst)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times T$ vector containing the single-channel mixture signal\\
\texttt{S} & $J\times T$ matrix containing the target signals (e.g. source images)\\
\texttt{L} & length of the STFT window in samples (must be a multiple of 4)\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\texttt{mreal} & \texttt{true} for real-valued masking (default), \texttt{false} for binary masking\\
\texttt{mconst} & \texttt{true} when the masks are subject to a unitary sum constraint (default), \texttt{false} otherwise\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the near-optimal estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $\left(\frac{L}{2}+1\right)\times N\times J$ table containing near-optimal masks with $N=\ceil\left(\frac{T}{M}\right)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 5.4.
\newpage

\mysection{bss\_nearopt\_binmask}
Near-optimal constrained binary time-frequency masks for multichannel source separation using STFT with sine window (coefficients derived for each time-frequency point separately).\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_nearopt\_binmask(X,S,L)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_binmask(X,S,L,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $I\times T\times J$ table containing the target signals (source images)\\
\texttt{L} & length of the STFT window in samples (must be a multiple of 4)\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $I\times T\times J$ table containing the near-optimal estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $\left(\frac{L}{2}+1\right)\times N\times J$ table containing near-optimal masks with $N=\ceil\left(\frac{T}{M}\right)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 6.4.
\newpage

\mysection{bss\_nearopt\_pinvmask}
Near-optimal estimator for multichannel source separation of possibly convolutive mixtures by time-frequency masking and mixing matrix pseudo-inversion using STFT with sine window (activity patterns derived for each time-frequency point separately).\\[1em]
\mysubsection{Syntax}
\texttt{[Se,W,SDR]=bss\_nearopt\_pinvmask(X,S,A)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_pinvmask(X,S,A,Ja)}\\
\texttt{[Se,W,SDR]=bss\_nearopt\_pinvmask(X,S,A,Ja,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{S} & $I\times T\times J$ table containing the target signals (source images)\\
\texttt{A} & $I\times J\times \left(\frac{L}{2}+1\right)$ table containing complex mixing matrices for positive frequencies (may be different from the ones actually used to generate $S$), with $L$ being the length of the STFT window in samples\\
\texttt{Ja} & number of active sources per time-frequency point (by default or if $Ja=0$, the best number is estimated for each time-frequency point)\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $I\times T\times J$ table containing the near-optimal estimates of the target signals (truncated to the same time range as the original signals)\\
\texttt{W} & $\left(\frac{L}{2}+1\right)\times N\times J$ table of binary coefficients indicating the oracle source activity patterns with $N=\ceil\left(\frac{T}{M}\right)$\\
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels (before truncation of the target estimates)\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 6.4.
\newpage

\section{Time-frequency transforms}
\label{sec:tf}
\bigskip

\mysection{mdct}
Modified Discrete Cosine Transform using a sine window.\\[1em]
\mysubsection{Syntax}
\texttt{X=mdct(x,L)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times T$ vector containing a single-channel signal\\
\texttt{L} & length of the MDCT window in samples (must be a multiple of 4)\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $\frac{L}{2}\times N$ matrix containing the MDCT coefficients with $N=\ceil\left(\frac{2T}{L}\right)$\\
\end{tabularx}
\newpage

\mysection{imdct}
Inverse Modified Discrete Cosine Transform using a sine window.\\[1em]
\mysubsection{Syntax}
\texttt{x=imdct(X)}\\[1em]
\mysubsection{Input}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $\frac{L}{2}\times N$ matrix containing a set of MDCT coefficients\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times \frac{NL}{2}$ vector containing the inverse MDCT signal\\
\end{tabularx}\\[1em]
If \texttt{x} is a signal of length \texttt{T}, \texttt{X=mdct(x,L)} and \texttt{y=imdct(X)}, then \texttt{x=y(1:T)}.
\newpage

\mysection{stft}
Short-Term Fourier Transform using a sine window.\\[1em]
\mysubsection{Syntax}
\texttt{X=stft(x,L)}\\
\texttt{X=stft(x,L,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times T$ vector containing a single-channel signal\\
\texttt{L} & length of the STFT window in samples (must be a multiple of 4)\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $\left(\frac{L}{2}+1\right)\times N$ matrix containing the STFT coefficients for positive frequencies with $N=\ceil\left(\frac{T}{M}\right)$\\
\end{tabularx}
\newpage

\mysection{istft}
Inverse Short-Term Fourier Transform using a sine window.\\[1em]
\mysubsection{Syntax}
\texttt{x=istft(X)}\\
\texttt{x=istft(X,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $\left(\frac{L}{2}+1\right)\times N$ matrix containing a set of STFT coefficients for positive frequencies\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{x} & $1\times NM$ vector containing the inverse STFT signal\\
\end{tabularx}\\[1em]
If \texttt{x} is a signal of length \texttt{T}, \texttt{X=stft(x,L)} and \texttt{y=istft(X)}, then \texttt{x=y(1:T)}.
\newpage

\section{Filtering and masking functions}
\label{sec:func}
\bigskip

\mysection{apply\_multifilt\_temp}
Apply time-domain demixing filters.\\[1em]
\mysubsection{Syntax}
\texttt{Se=apply\_multifilt\_temp(X,W)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{W} & $J\times I\times L$ table containing the coefficients of the demixing filters (delays from $-\frac{L}{2}+1$ to $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times (T+L-1)$ matrix containing the demixed signals\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 4.1.
\newpage

\mysection{apply\_multifilt\_freq}
Apply frequency-domain demixing matrices using STFT with sine window.\\[1em]
\mysubsection{Syntax}
\texttt{Se=apply\_multifilt\_freq(X,W)}\\
\texttt{Se=apply\_multifilt\_freq(X,W,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{W} & $J\times I\times \left(\frac{L}{2}+1\right)$ table containing complex demixing matrices for positive frequencies, with $L$ being the length of the STFT window in samples\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times NM$ matrix containing the demixed signals with $N=\ceil\left(\frac{T}{M}\right)$\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 4.4.
\newpage

\mysection{apply\_pinvmask\_inst}
Apply multichannel time-frequency masks with mixing matrix pseudo-inversion using MDCT with sine window.\\[1em]
\mysubsection{Syntax}
\texttt{Se=apply\_pinvmask\_inst(X,A,W)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{A} & $I\times J$ real-valued mixing matrix\\
\texttt{W} & $\frac{L}{2}\times N\times J$ table of binary coefficients indicating the source activity patterns with $L$ being the length of the MDCT window in samples and $N=\ceil\left(\frac{2T}{L}\right)$\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times \frac{NL}{2}\times J$ table containing the derived source images\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 6.1.2.
\newpage

\mysection{apply\_pinvmask\_conv}
Apply multichannel time-frequency masks with mixing matrix pseudo-inversion using STFT with sine window.\\[1em]
\mysubsection{Syntax}
\texttt{Se=apply\_pinvmask\_conv(X,A,W)}\\
\texttt{Se=apply\_pinvmask\_conv(X,A,W,M)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{X} & $I\times T$ matrix containing the multichannel mixture signal\\
\texttt{A} & $I\times J\times \left(\frac{L}{2}+1\right)$ table containing complex mixing matrices for positive frequencies, with $L$ being the length of the STFT window in samples\\
\texttt{W} & $\frac{L}{2}\times N\times J$ table of binary coefficients indicating the source activity patterns with $N=\ceil\left(\frac{2T}{L}\right)$\\
\texttt{M} & step between successive windows in samples (must be a multiple of 2, a divider of $L$ and smaller than $\frac{L}{2}$) (default: $\frac{L}{2}$)\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times NM\times J$ table containing the derived source images\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 6.4.
\newpage

\section{Auxiliary functions}
\label{sec:aux}
\bigskip

\mysection{optim\_coeffs}
Oracle constrained real-valued masking coefficients for a single basis element.\\[1em]
\mysubsection{Syntax}
\texttt{[wo,disto]=optim\_coeffs(r)}\\[1em]
\mysubsection{Input}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{r} & $J\times 1$ vector containing ratios between MDCT, CP or WP coefficients or real parts of ratios between STFT coefficients of the targets and the mixture for a single basis element\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{wo} & oracle masking coefficients for this basis element\\
\texttt{disto} & achieved distortion\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 5.2.
\newpage

\mysection{pinv\_filt}
Pseudo-inversion of a filter system.\\[1em]
\mysubsection{Syntax}
\texttt{[W,B,SIR]=pinv\_filt(A,zdel,L)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{A} & $I\times J\times T$ table containing filters of length $T$ (delays from $-zdel+1$ to $T-zdel$)\\
\texttt{zdel} & sample index corresponding to zero delay\\
\texttt{L} & length of the pseudo-inverse filters in samples\\
\end{tabularx}\\[1em]
\mysubsection{Outputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{W} & $J\times I\times L$ table containing pseudo-inverse filters (delays from $-\frac{L}{2}+1$ to $\frac{L}{2}$)\\
\texttt{B} & $J\times J\times (T+L-1)$ product of $W$ and $A$ (delays from $-zdel-\frac{L}{2}+2$ to $T-zdel+\frac{L}{2}$)\\
\texttt{SIR} & achieved $\mathrm{SIR}$ in deciBels\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin06} Section 4.3.3.
\newpage

\mysection{sdr}
Signal to Distortion Ratio.\\[1em]
\mysubsection{Syntax}
\texttt{SDR=sdr(Se,S)}\\[1em]
\mysubsection{Inputs}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{Se} & $J\times T$ matrix containing the estimated signals\\
\texttt{S} & $J\times T$ matrix containing the target signals\\
\end{tabularx}\\[1em]
\mysubsection{Output}
\begin{tabularx}{\textwidth}{p{2cm}X}
\texttt{SDR} & achieved $\mathrm{SDR}$ in deciBels\\
\end{tabularx}\\[1em]
\mysubsection{Reference}
See \cite{Vin07a} Section 2.3.
\newpage

\chapter{Example data and applications}
\label{cha:examples}
The full version of BSS Oracle contains example sources, filters and scripts that were used to plot the figures of the reference publications.

\section{Sources and filters}
\label{sec:data}
\begin{tabularx}{\textwidth}{p{5cm}X}
\texttt{data/mix$k$\_s$j$.wav} & source $j$ of mixture $k$, with $1\leq j\leq 3$ (music for $1\leq k\leq 10$, speech for $11\leq k\leq 20$)\\
\texttt{data/ir\_$t$.mat} & mixing impulse responses with reverberation time $t$ equal to \texttt{anechoic}, \texttt{50ms}, \texttt{250ms} or \texttt{1.25s} with source 1 at -40$^\circ$\\
\texttt{data/ir\_move1\_250ms.mat} & mixing impulse responses with 250 ms reverberation time with source 1 at -38$^\circ$\\
\texttt{data/ir\_move2\_250ms.mat} & mixing impulse responses with 250 ms reverberation time with source 1 at -36$^\circ$\\
\texttt{data/ir\_move3\_250ms.mat} & mixing impulse responses with 250 ms reverberation time with source 1 at -32$^\circ$\\
\end{tabularx}\\[1em]
See \cite{Vin07a} Section 3.

\section{Scripts}
\label{sec:figures}
\begin{tabularx}{\textwidth}{p{5cm}X}
\texttt{multifilt1.m} & plots figure 2 of \cite{Vin07a}\\
\texttt{multifilt2.m} & plots figure 3 of \cite{Vin07a}\\
\texttt{multifilt3.m} & plots figure 4 of \cite{Vin06}\\
\texttt{multifilt4.m} & plots figure 4 of \cite{Vin07a}\\
\texttt{monomask1.m} & plots figure 5 of \cite{Vin07a}\\
\texttt{monomask2.m} & plots figure 7 of \cite{Vin06}\\
\texttt{monomask3.m} & plots figure 6 of \cite{Vin07a}\\
\texttt{multimask1.m} & plots figure 7 of \cite{Vin07a}\\
\texttt{multimask2.m} & plots figure 8 of \cite{Vin07a}\\
\texttt{bbasis\_monomask1.m} & plots figure 11 of \cite{Vin06}\\
\texttt{bbasis\_monomask2.m} & plots figure 12 of \cite{Vin06}\\
\texttt{bbasis\_monomask3.m} & plots figures 13 and 14 of \cite{Vin06}\\
\texttt{robust1.m} & plots figure 9 of \cite{Vin07a}\\
\texttt{robust2.m} & plots figure 10 of \cite{Vin07a}\\
\end{tabularx}
\newpage$$ $$\newpage

\addcontentsline{toc}{chapter}{Bibliography}
\bibliographystyle{plain}
\bibliography{user_guide}

\end{document}