function SDR=sdr(Se,S)

% SDR Signal to Distortion Ratio.
%
% SDR=sdr(Se,S)
%
% Inputs:
% Se: J x T matrix containing the estimated signals
% S: J x T matrix containing the target signals
%
% Output:
% SDR: achieved SDR in deciBels
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 2.3.

%%% Errors and warnings %%%
if nargin<2, error('Not enough input arguments.'); end
[J,T]=size(S);
[Je,Te]=size(Se);
if (T~=Te) || (J~=Je), error('Target and estimated signals must have the same size.'); end

%%% Computing SDR %%%
SDR=10*log10(sum(sum(S.^2))/sum(sum((Se-S).^2)));