function [wo,disto]=optim_coeffs(r)

% OPTIM_COEFFS Oracle constrained real-valued masking coefficients for a single
% basis element.
%
% [wo,disto]=optim_coeffs(r)
%
% Input:
% r: J x 1 vector containing ratios between MDCT, CP or WP coefficients or real
%    parts of ratios between STFT coefficients of the targets and the mixture
%    for a single basis element
%
% Outputs:
% wo: oracle masking coefficients for this basis element
% disto: achieved distortion
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 5.2.

%%% Errors and warnings %%%
if nargin<1, error('Not enough input arguments.'); end
[J,T]=size(r);
if T>1, error('The input must be a column vector.'); end

if (J > 6) && (exist('quadprog','file') == 2),
    %%% Using quadprog (MATLAB Optimization Toolbox) %%%
    options=optimset('Display','off');
    wo=quadprog(eye(J),-r,-eye(J),zeros(J,1),ones(1,J),1,[],[],[],options);

else
    %%% Using full combinatorial search %%%
    % Initializing activity patterns for J sources
    act=zeros(2^J,J);
    for p=1:J,
        act(:,p)=reshape(repmat([0 1],[2^(J-p) 2^(p-1)]),2^J,1);
    end
    disto=inf;
    for nbzer=0:J-1,
        % Testing activity patterns when the last nbzer coefficients are zero
        act=act(1:2^(J-nbzer-1),2:J-nbzer);
        for p=1:2^(J-nbzer-1),
            % Computing solution of unconstrained least-squares problem
            ind=find(act(p,:));
            nind=length(ind);
            B=eye(nind)-ones(nind)/(nind+1);
            D=ones(nind,1)+r(ind,1)-r(J-nbzer,1);
            wu=B*D;
            % Checking whether unconstrained solution verifies the constraints
            if all(wu >= 0) && sum(wu) <= 1,
                w=zeros(J,1);
                w(ind)=wu;
                w(J-nbzer)=1-sum(wu);
                % Computing relative distortion
                rerror=w-r;
                dist=rerror.'*rerror;
                % Saving result if best so far
                if dist <= disto,
                    disto=dist;
                    wo=w;
                end
            end
        end
    end
    
end

return;