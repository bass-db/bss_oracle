function Se=apply_multifilt_temp(X,W)

% APPLY_MULTIFILT_TEMP Apply time-domain demixing filters.
%
% Se=apply_multifilt_temp(X,W)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% W: J x I x L table containing the coefficients of the demixing filters
%    (delays from -L/2+1 to L/2)
%
% Output:
% Se: J x (T+L-1) matrix containing the demixed signals
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 4.1.

%%% Errors and warnings %%%
if nargin<2, error('Not enough input arguments.'); end
[I,T]=size(X);
[J,Iw,L]=size(W);
if I>T, error('Mixture signals must be in rows.'); end
if I~=Iw, error('The number of colums of the demixing filter matrix must be equal to the number of mixture signals.'); end
if L~=2*floor(L/2), error('The length of the demixing filters must be even.'); end

%%% Applying demixing filters %%%
X=[X,zeros(I,L-1)];
Se=zeros(J,T+L-1);
for j=1:J,
    Se(j,:)=sum(fftfilt(reshape(W(j,:,:),I,L).',X.').',1);
end

return;
