function [Se,W,SDR]=bss_nearopt_binmask(X,S,L,M)

% BSS_NEAROPT_BINMASK Near-optimal constrained binary time-frequency
% masks for multichannel source separation using STFT with sine window
% (coefficients derived for each time-frequency point separately).
%
% [Se,W,SDR]=bss_nearopt_binmask(X,S,L)
% [Se,W,SDR]=bss_nearopt_binmask(X,S,L,M)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: I x T x J matrix containing the target signals (source images)
% L: length of the STFT window in samples (must be a multiple of 4)
% M: step between successive windows in samples (must be even, a divider of
%    L and smaller than L/2) (default: L/2)
%
% Outputs:
% Se: I x T x J matrix containing the near-optimal estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: (L/2+1) x N x J table containing near-optimal masks with N=ceil(T/M)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 6.4.
%
% See also stft, istft.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
if nargin<4, M=L/2; end
[I,T]=size(X);
[Is,Ts,J]=size(S);
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if I~=Is, error('Mixture and target signals have a different number of channels.'); end
if any(any(abs(X-sum(S,3)) > eps)), error('The mixture signal must be equal to the sum of the target signals.'); end
N=ceil(T/M);

%%% Computing STFT of input data %%%
XX=zeros(L/2+1,N,I);
SS=zeros(L/2+1,N,I,J);
for i=1:I,
    XX(:,:,i)=stft(X(i,:),L,M);
    for j=1:J,
        SS(:,:,i,j)=stft(S(i,:,j),L,M);
    end
end

%%% Computing near-optimal masks %%%
W=zeros(L/2+1,N,J);
% Distortion depending on the selected source
disto=zeros(L/2+1,N,J);
for j=1:J,
    disto(:,:,j)=sum(abs(SS(:,:,:,j)-XX).^2,3)+sum(sum(abs(SS(:,:,:,[1:j-1 j+1:J])).^2,4),3);
end
% Constrained binary mask
[disto,ind]=min(disto,[],3);
W((1:L/2+1).'*ones(1,N)+ones(L/2+1,1)*(L/2+1)*(0:N-1)+(ind-1)*N*(L/2+1))=1;

%%% Deriving estimates of the target signals %%%
% Applying the masks
Se=zeros(I,N*M,J);
for i=1:I,
    for j=1:J,
        Se(i,:,j)=istft(XX(:,:,i).*W(:,:,j),M);
    end
end
% SDR
SDR=sdr(reshape(Se,I*N*M,J),reshape(cat(2,S,zeros(I,N*M-T,J)),I*N*M,J));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;
