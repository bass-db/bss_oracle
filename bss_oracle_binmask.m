function [Se,W,SDR]=bss_oracle_binmask(X,S,L)

% BSS_ORACLE_BINMASK Oracle estimator for multichannel source separation
% by constrained binary time-frequency masking using MDCT with sine window.
%
% [Se,W,SDR]=bss_oracle_binmask(X,S,L)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: I x T x J table containing the target signals (source images)
% L: length of the MDCT window in samples (must be a multiple of 4)
%
% Outputs:
% Se: I x T x J table containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: L/2 x N x J table containing the oracle masks with N=ceil(T/L*2)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 6.2.
%
% See also mdct, imdct.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
[I,T]=size(X);
[Is,Ts,J]=size(S);
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if I~=Is, error('Mixture and target signals have a different number of channels.'); end
if any(any(abs(X-sum(S,3)) > eps)), error('The mixture signal must be equal to the sum of the target signals.'); end
N=ceil(T/L*2);

%%% Computing MDCT of input data %%%
XX=zeros(L/2,N,I);
SS=zeros(L/2,N,I,J);
for i=1:I,
    XX(:,:,i)=mdct(X(i,:),L);
    for j=1:J,
        SS(:,:,i,j)=mdct(S(i,:,j),L);
    end
end

%%% Computing oracle masks %%%
W=zeros(L/2,N,J);
% Distortion depending on the selected source
disto=zeros(L/2,N,J);
for j=1:J,
    disto(:,:,j)=sum((SS(:,:,:,j)-XX).^2,3)+sum(sum(SS(:,:,:,[1:j-1 j+1:J]).^2,4),3);
end
% Constrained binary mask
[disto,ind]=min(disto,[],3);
W((1:L/2).'*ones(1,N)+ones(L/2,1)*L/2*(0:N-1)+(ind-1)*N*L/2)=1;

%%% Deriving oracle estimates of the target signals %%%
% Applying oracle masks
Se=zeros(I,N*L/2,J);
for i=1:I,
    for j=1:J,
        Se(i,:,j)=imdct(XX(:,:,i).*W(:,:,j));
    end
end
% SDR
SDR=sdr(reshape(Se,I*N*L/2,J),reshape(cat(2,S,zeros(I,N*L/2-T,J)),I*N*L/2,J));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;
