function X=mdct(x,L)

% MDCT Modified Discrete Cosine Transform using a sine window.
%
% X=mdct(x,L)
%
% Inputs:
% x: 1 x T vector containing a single-channel signal
% L: length of the MDCT window in samples (must be a multiple of 4)
%
% Output:
% X: L/2 x N matrix containing the MDCT coefficients with N=ceil(T/L*2)
%
% See also imdct.

%%% Errors and warnings %%%
if nargin<2, error('Not enough input arguments.'); end
[I,T]=size(x);
if I>1, error('The input signal must be a row vector.'); end
if L~=4*floor(L/4), error('The MDCT window length must be a multiple of 4.'); end

%%% Computing MDCT coefficients %%%
% Defining sine window
win=sin((.5:L-.5)/L*pi).';
% Zero-padding and edge folding
N=ceil(T/L*2);
x=[x,zeros(1,N*L/2-T)];
x=[-x(L/4:-1:1).*(1-win(L/2+1:3*L/4).')./win(1:L/4).',x,x(N*L/2:-1:N*L/2-L/4+1).*(1-win(L/4+1:L/2).')./win(3*L/4+1:L).'];
X=zeros(L/2,N);
for t=0:N-1,
    % Framing
    frame=x(t*L/2+1:t*L/2+L).'.*win;
    % DCT-IV
    X(:,t+1)=dct_iv([-frame(3*L/4:-1:L/2+1)-frame(3*L/4+1:L);frame(1:L/4)-frame(L/2:-1:L/4+1)]).';
end

return;