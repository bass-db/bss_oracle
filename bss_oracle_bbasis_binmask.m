function [Se,btree,W,SDR,stree]=bss_oracle_bbasis_binmask(X,S,Dmin,Dmax,pcos)

% BSS_ORACLE_BBASIS_BINMASK Oracle estimator for multichannel source separation
% by binary time-frequency masking using the best CP/WP basis.
%
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_binmask(X,S,Dmin,Dmax)
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_binmask(X,S,Dmin,Dmax,pcos)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: I x T x J table containing the target signals (source images)
% Dmin: minimal packet depth
% Dmax: maximal packet depth
% pcos: true for CP basis with sine window (default)
%       false for WP basis with symmlet-8
%
% Outputs:
% Se: I x T x J table containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% btree: 1 x (2^(Dmax+1)-1) vector of binary values representing the tree
%        structure corresponding to the oracle best basis
% W: 2^N x (Dmax-Dmin+1) x J table containing the oracle masks for each scale
%    with N=nextpow2(T)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
% stree: 1 x (2^(Dmax+1)-1) vector containing the oracle distortion for all basis
%        elements (infinite for disallowed scales)
%
% Reference:
% E. Vincent and R. Gribonval, "Blind criterion and oracle bound for
% instantaneous audio source separation using adaptive time-frequency
% representations," in Proc. IEEE Workshop on Applications of Signal
% Processing to Audio and Acoustics (WASPAA), 2007.
%
% See also CPAnalysis, CPSynthesis, WPAnalysis, WPSynthesis.

%%% Errors and warnings %%%
if nargin<4, error('Not enough input arguments.'); end
if nargin<5, pcos=true; end
[I,T]=size(X);
[Is,Ts,J]=size(S);
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if I~=Is, error('Mixture and target signals have a different number of channels.'); end
if any(any(abs(X-sum(S,3)) > eps)), error('The mixture signal must be equal to the sum of the target signals.'); end
if Dmin>Dmax, error('The minimal depth must be smaller than the maximal depth.'); end
N=nextpow2(T);

%%% Computing input data transforms %%%
% Zero-padding
X=[X,zeros(I,2^N-T)];
S=cat(2,S,zeros(I,2^N-T,J));
XX=zeros(2^N,Dmax-Dmin+1,I);
SS=zeros(2^N,Dmax-Dmin+1,I,J);
if pcos,
    % CP transform (all scales)
    for i=1:I,
        XXX=CPAnalysis(X(i,:),Dmax);
        XX(:,:,i)=XXX(:,Dmin+1:Dmax+1);
        for j=1:J,
            YYY=CPAnalysis(S(i,:,j),Dmax);
            SS(:,:,i,j)=YYY(:,Dmin+1:Dmax+1);
        end
    end
else
    % WP transform (all scales)
    for i=1:I,
        qmf=MakeONFilter('Symmlet',8);
        XXX=WPAnalysis(X(i,:),Dmax,qmf);
        XX(:,:,i)=XXX(:,Dmin+1:Dmax+1);
        for j=1:J,
            YYY=WPAnalysis(S(i,:,j),Dmax,qmf);
            SS(:,:,i,j)=YYY(:,Dmin+1:Dmax+1);
        end
    end
end

%%% Computing oracle mask and distortion for each element and each scale %%%
W=zeros(2^N,Dmax-Dmin+1,J);
stree=zeros(1,2^(Dmax+1)-1);
% Distortion depending on the selected source
disto=zeros(2^N,Dmax-Dmin+1,J);
for j=1:J,
    disto(:,:,j)=sum((SS(:,:,:,j)-XX).^2,3)+sum(sum(SS(:,:,:,[1:j-1 j+1:J]).^2,4),3);
end
% Constrained binary mask
[disto,ind]=min(disto,[],3);
W((1:2^N).'*ones(1,Dmax-Dmin+1)+ones(2^N,1)*2^N*(0:Dmax-Dmin)+(ind-1)*(Dmax-Dmin+1)*2^N)=1;
for d=0:Dmax,
    if d < Dmin,
        % Infinite distortion for disallowed scales
        stree(node(d,0:2^d-1))=inf;
    else
        for b=0:(2^d-1),
            % Storing total distortion
            stree(node(d,b))=sum(disto(packet(d,b,2^N),d-Dmin+1));
        end
    end
end

%%% Selecting best basis %%%
global WLVERBOSE;
WLVERBOSE='No';
btree=BestBasis(stree,Dmax);

%%% Deriving oracle estimates of the target signals %%%
Se=zeros(I,2^N,J);
for i=1:I,
    for j=1:J,
        % Applying oracle mask
        YYY=[zeros(2^N,Dmin),XX(:,:,i).*W(:,:,j)];
        if pcos,
            % Inverse CP transform (using best basis only)
            Se(i,:,j)=CPSynthesis(btree,YYY);
        else
            % Inverse WP transform (using best basis only)
            Se(i,:,j)=WPSynthesis(btree,YYY,qmf);
        end
    end
end
% SDR
SDR=sdr(reshape(Se,I*2^N,J),reshape(S,I*2^N,J));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;