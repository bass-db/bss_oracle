function [Se,W,SDR]=bss_nearopt_multifilt(X,S,L,M)

% BSS_NEAROPT_MULTIFILT Near-optimal demixing matrices for source separation
% by frequency-domain multichannel time-invariant filtering using STFT with
% sine window (coefficients derived for each frequency bin separately).
%
% [Se,W,SDR]=bss_nearopt_multifilt(X,S,L)
% [Se,W,SDR]=bss_nearopt_multifilt(X,S,L,M)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: J x T matrix containing the target signals (e.g. sources or source images)
% L: length of the STFT window in samples (must be a multiple of 4)
% M: step between successive windows in samples (must be even, a divider of
%    L and smaller than L/2) (default: L/2)
%
% Outputs:
% Se: J x T matrix containing the near-optimal estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: J x I x (L/2+1) table containing near-optimal demixing matrices
%     for positive frequencies
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 4.4.
%
% See also stft, istft.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
if nargin<4, M=L/2; end
[I,T]=size(X);
[J,Ts]=size(S);
if I>T, error('Mixture signals must be in rows.'); end
if J>Ts, error('Target signals must be in rows.'); end
if T~=Ts, error('Mixture and target signals have different lengths.'); end
N=ceil(T/M);

%%% Computing STFT of input data %%%
Xf=zeros(L/2+1,N,I);
for i=1:I,
    Xf(:,:,i)=stft(X(i,:),L,M);
end
Sf=zeros(L/2+1,N,J);
for j=1:J,
    Sf(:,:,j)=stft(S(j,:),L,M);
end

%%% Computing near-optimal demixing coefficients %%%
W=zeros(J,I,L/2+1);
for f=1:L/2+1,
    % Inner products between mixture channels
    G=reshape(Xf(f,:,:),N,I)'*reshape(Xf(f,:,:),N,I);
    % Inner products mixture channels and targets
    D=reshape(Xf(f,:,:),N,I)'*reshape(Sf(f,:,:),N,J);
    % Computing near-optimal demixing coefficients
    W(:,:,f)=(inv(G)*D).';
end

%%% Deriving estimates of the target signals %%%
% Applying demixing matrices
Se=apply_multifilt_freq(X,W,M);
% SDR
SDR=sdr(Se,[S,zeros(J,N*M-T)]);
% Truncating target signal estimates to original target length
Se=Se(:,1:T);

return;
