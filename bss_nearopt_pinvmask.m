function [Se,W,SDR]=bss_nearopt_pinvmask(X,S,A,Ja,M)

% BSS_NEAROPT_PINVMASK Near-optimal estimator for multichannel source
% separation of possibly convolutive mixtures by time-frequency masking and
% mixing matrix pseudo-inversion using STFT with sine window
% (activity patterns derived for each time-frequency point separately).
%
% [Se,W,SDR]=bss_nearopt_pinvmask(X,S,A)
% [Se,W,SDR]=bss_nearopt_pinvmask(X,S,A,Ja)
% [Se,W,SDR]=bss_nearopt_pinvmask(X,S,A,Ja,M)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: I x T x J table containing the target signals (source images)
% A: I x J x (L/2+1) table containing complex mixing matrices for positive
%     frequencies (may be different from the ones actually used to generate S),
%     with L being the length of the STFT window in samples
% Ja: number of active sources per time-frequency point (by default or if Ja=0,
%      the best number is estimated for each time-frequency point)
% M: step between successive windows in samples (must be even, a divider of
%    L and smaller than L/2) (default: L/2)
%
% Outputs:
% Se: I x T x J table containing the near-optimal estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: (L/2+1) x N x J table of binary coefficients indicating near-optimal
% source activity patterns with N=ceil(T/M)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 6.4.
%
% See also stft, istft.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
if nargin<4, Ja=0; end
[I,T]=size(X);
[Is,Ts,J]=size(S);
[Im,Jm,L]=size(A);
if L==2*floor(L/2), error('The number of positive frequency channels must be odd.'); end
L=2*(L-1);
if nargin<5, M=L/2; end
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if I~=Is, error('Mixture and target signals have a different number of channels.'); end
if I~=Im, error('The number of rows in the mixing matrices must be equal to the number of mixture signals.'); end
if J~=Jm, error('The number of columns in the mixing matrices must be equal to the number of sources.'); end
if Ja>J, error('The number of active sources must be smaller than the number of sources.'); end
if any(any(abs(X-sum(S,3)) > eps)), error('The mixture signal must be equal to the sum of the target signals.'); end
N=ceil(T/M);

%%% Computing STFT of input data %%%
XX=zeros(L/2+1,N,I);
SS=zeros(L/2+1,N,I,J);
for i=1:I,
    XX(:,:,i)=stft(X(i,:),L,M);
    for j=1:J,
        SS(:,:,i,j)=stft(S(i,:,j),L,M);
    end
end

%%% Selecting the best active sources %%%
% Source activity patterns
act=zeros(2^J,J);
for p=1:J,
    act(:,p)=reshape(repmat([0 1],[2^(J-p) 2^(p-1)]),2^J,1);
end
% Distortion for each pattern
disto=zeros(L/2+1,N,2^J);
for p=1:2^J,
    ind=find(act(p,:));
    nind=length(ind);
    if (nind==Ja) || (Ja==0),
        % Pseudo-inversion of mixing matrices for active sources
        for f=1:L/2+1,
            B=pinv(A(:,ind,f));
            SSe=zeros(N,I,J);
            for j=1:nind,
                SSe(:,:,ind(j))=reshape(XX(f,:,:),N,I)*B(j,:).'*A(:,ind(j),f).';
            end
            disto(f,:,p)=sum(sum(abs(SSe-reshape(SS(f,:,:,:),N,I,J)).^2,3),2);
        end
    else
        % Infinite distortion for disallowed number of active sources
        disto(:,:,p)=Inf;
    end
end
% Selecting best pattern and deriving near-optimal target STFTs
[disto,pat]=min(disto,[],3);
W=zeros(L/2+1,N,J);
SSe=zeros(L/2+1,N,I,J);
for p=1:2^J,
    ind=find(act(p,:));
    nind=length(ind);
    for f=1:L/2+1,
        n=find(pat(f,:)==p);
        W(f,n,ind)=1;
        B=pinv(A(:,ind,f));
        for j=1:nind,
            SSe(f,n,:,ind(j))=reshape(reshape(XX(f,n,:),length(n),I)*B(j,:).'*A(:,ind(j),f).',1,length(n),I);
        end
    end
end

%%% Deriving estimates of the target signals %%%
% Target signals
Se=zeros(I,N*M,J);
for i=1:I,
    for j=1:J,
        Se(i,:,j)=istft(SSe(:,:,i,j),M);
    end
end
% SDR
SDR=sdr(reshape(Se,I*N*M,J),reshape(cat(2,S,zeros(I,N*M-T,J)),I*N*M,J));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;
