function x=imdct(X)

% IMDCT Inverse Modified Discrete Cosine Transform using a sine window.
%
% x=imdct(X)
%
% Input:
% X: L/2 x N matrix containing a set of MDCT coefficients
%
% Output:
% x: 1 x (N*L/2) vector containing the inverse MDCT signal
%
% If x is a signal of length T, X=mdct(x,L) and y=imdct(X), then x=y(1:T).
%
% See also mdct.

%%% Errors and warnings %%%
if nargin<1, error('Not enough input arguments.'); end
[L,N]=size(X);
if L~=2*floor(L/2), error('The number of rows the MDCT matrix must be even.'); end
L=L*2;

%%% Computing inverse MDCT signal %%%
% Defining sine window
win=sin((.5:L-.5)/L*pi);
T=N*L/2;
x=zeros(1,T+L/2);
for t=0:N-1,
    % DCT-IV
    frame=dct_iv(X(:,t+1));
    % Overlap-add
    x(t*L/2+1:t*L/2+L)=x(t*L/2+1:t*L/2+L)+[frame(L/4+1:L/2),-frame(L/2:-1:L/4+1),-frame(L/4:-1:1),-frame(1:L/4)].*win;
end
% Edge unfolding and truncating
x(L/4+1:L/2)=x(L/4+1:L/2)./win(L/4+1:L/2);
x(T+1:T+L/4)=x(T+1:T+L/4)./win(L/2+1:3*L/4);
x=x(L/4+1:L/4+T);

return;