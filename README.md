This repository contains version 2.1 of BSS Oracle dated from October 12, 2007, and previously available at http://bass-db.gforge.inria.fr/bss_oracle/.


# Purpose
BSS Oracle is a Matlab toolbox to **evaluate the best performance** achievable by a class of source separation algorithms in an evaluation framework where **reference source signals are known**. In other words, it computes the **best demixing filters** or the **best time-frequency masks** for a given mixture signal.

BSS Oracle implements oracle estimators for four classes of separation algorithms:
- multichannel time-invariant filtering,
- single-channel time-frequency masking,
- multichannel time-frequency masking,
- best basis masking.

BSS Oracle cannot separate sources blindly: it needs reference source signals to select the best separation parameters.


# Usage
Read Sections 2 and 3 of the [user guide](doc/user_guide.pdf).

Some functions of BSS Oracle (involving MDCT, CP or WP transforms) depend on the [Wavelab toolbox version 802](http://statweb.stanford.edu/~wavelab/index_wavelab802.html).

# Reproducibility
The toolbox comes with the source signals and the mixing filters used to plot the figures in [[1]](#1). To reproduce these figures, read Section 4 of the [user guide](doc/user_guide.pdf).


# References
<a id="1">[1]</a> E. Vincent, R. Gribonval, and M.D. Plumbley, ["Oracle estimators for the benchmarking of source separation algorithms"](https://hal.inria.fr/inria-00544194/document), *Signal Processing* 87(8), p. 1933-1950, 2007.  
[2] E. Vincent and R. Gribonval, ["Blind criterion and oracle bound for instantaneous audio source separation using adaptive time-frequency representations"](https://hal.inria.fr/inria-00544207/document), in *Proc. IEEE Workshop on Applications of Signal Processing to Audio and Acoustics (WASPAA)*, 2007.  
[3] E. Vincent, R. Gribonval, and M.D. Plumbley, [BSS Oracle Toolbox Version 2.1 - User Guide](doc/user_guide.pdf), 2007.


# License

The files contained in the main directory and in the subdirectories doc/ and examples/ are authored by Emmanuel Vincent, Rémi Gribonval and Mark D. Plumbley and distributed under the terms of the [GNU General Public License (GPL) version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).

The music sound files contained in the subdirectory data/ are distributed under specific Creative Commons licenses. For more details about the license applying to each file, see [data/LICENSES.txt](data/LICENSES.txt).

All other files of the subdirectory data/ are license free.