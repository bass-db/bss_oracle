% MULTIMASK2
%
% Plots figure 8 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of multichannel near-optimal time-frequency masks on
% two-channel three-source mixtures with reverberation time RT = 250 ms as
% a function of STFT length L. Plain: local mixing inversion with a free
% number of active sources J'(n,f) for each time-frequency point (n,f).
% Dashed: J' = 2 active sources. Dash-dotted: J' = 1 active source. Dotted:
% binary masking.

load('ir_250ms.mat');
A=A(1:2,:,:); T=5506;
L=[4 8 4*round(2.^((15:56)/4-2))];
SDR=zeros(44,4,20);

% Different numbers of active sources
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    s3=wavread(['mix' num '_s3.wav']).'; s13=fftfilt(A(1,3,:),[s3,zeros(1,64)]); s23=fftfilt(A(2,3,:),[s3,zeros(1,64)]);
    X=[s11+s12+s13;s21+s22+s23]; X=X(:,65:2^18+64); S=zeros(2,2^18+64,3); S(:,:,1)=[s11;s21]; S(:,:,2)=[s12;s22]; S(:,:,3)=[s13;s23]; S=S(:,65:2^18+64,:); clear s1 s2 s3 s11 s21 s12 s22 s13 s23;
    for l=1:44,
        D=exp(2*i*pi*64/L(l)*(0:L(l)-1)).';
        Af=sum(reshape(cat(3,A,zeros(2,3,ceil(T/L(l))*L(l)-T)),2,3,L(l),ceil(T/L(l))),4);
        Af=permute(fft(permute(Af,[3 1 2])).*repmat(D,[1 2 3]),[2 3 1]);
        Af=Af(:,:,1:L(l)/2+1);
        [Se,W,SDR(l,1,m)]=bss_nearopt_pinvmask(X,S,Af);
        [Se,W,SDR(l,2,m)]=bss_nearopt_pinvmask(X,S,Af,1);
        [Se,W,SDR(l,3,m)]=bss_nearopt_pinvmask(X,S,Af,2);
        [Se,W,SDR(l,4,m)]=bss_nearopt_binmask(X,S,L(l));
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'-k',L,SDR(:,2),'-.k',L,SDR(:,3),'--k',L,SDR(:,4),':k');
axis([4 16384 0 22.5]);
xlabel('L (samples)'); ylabel('SDR (dB)');