% MULTIFILT3
%
% Plots figure 4 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms,"
% Technical Report C4DM-TR-06-03, Queen Mary, University of London, 2006.
%
% Average performance of various source demixing filters as a function of
% their length L on determined two-source mixtures with reverberation time 
% RT = 250 ms. Plain: oracle filters applied to music data. Dashed: oracle
% filters applied to speech data. Dash-dotted: pseudo-inverse filters
% applied to music data. Dotted: pseudo-inverse filters applied to speech
% data.

load('ir_250ms.mat');
L=[2:2:14 2*round(2.^((16:44)/4-1))];
SDR=zeros(36,4,10);

% Speech oracle and pseudo-inverse
for m=1:10,
    num=['0' int2str(m+10)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22]; X=X(:,65:2^18+64); S=[s1;s2]; clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,1,m)]=bss_oracle_multifilt(X,S,L(l));
        [W,B,SIR]=pinv_filt(A(1:2,1:2,:),65,L(l));
        Se=apply_multifilt_temp(X,W);
        SDR(l,2,m)=sdr(Se,[zeros(2,L(l)/2-1),S,zeros(2,L(l)/2)]);
    end
end

% Music oracle and pseudo-inverse
for m=1:10,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22]; X=X(:,65:2^18+64); S=[s1;s2]; clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,3,m)]=bss_oracle_multifilt(X,S,L(l));
        [W,B,SIR]=pinv_filt(A(1:2,1:2,:),65,L(l));
        Se=apply_multifilt_temp(X,W);
        SDR(l,4,m)=sdr(Se,[zeros(2,L(l)/2-1),S,zeros(2,L(l)/2)]);
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'--k',L,SDR(:,2),':k',L,SDR(:,3),'-k',L,SDR(:,4),'-.k');
axis([2 2048 -2.5 25]);
xlabel('L (taps)'); ylabel('SDR (dB)');