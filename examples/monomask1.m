% MONOMASK1
%
% Plots figure 5 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of the constrained time-frequency masking oracles on
% single-channel two-source mixtures as a function of the length L of the
% MDCT basis elements. Plain: real-valued masks applied to music data.
% Dashed: real-valued masks applied to speech data. Dash-dotted: binary
% masks applied to music data. Dotted: binary masks applied to speech data.

L=[4 8 4*round(2.^((15:56)/4-2))];
SDR=zeros(44,4,10);

% Speech (binary and real-valued)
for m=1:10,
    num=['0' int2str(m+10)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x=s1+s2; S=[s1;s2]; clear s1 s2;
    for l=1:44,
        [Se,W,SDR(l,1,m)]=bss_oracle_monomask(x,S,L(l),false);
        [Se,W,SDR(l,2,m)]=bss_oracle_monomask(x,S,L(l));
    end
end

% Music (binary and real-valued)
for m=1:10,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x=s1+s2; S=[s1;s2]; clear s1 s2;
    for l=1:44,
        [Se,W,SDR(l,3,m)]=bss_oracle_monomask(x,S,L(l),false);
        [Se,W,SDR(l,4,m)]=bss_oracle_monomask(x,S,L(l));
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),':k',L,SDR(:,2),'--k',L,SDR(:,3),'-.k',L,SDR(:,4),'-k');
axis([4 16384 0 20]);
xlabel('L (samples)'); ylabel('SDR (dB)');
