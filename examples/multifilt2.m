% MULTIFILT2
%
% Plots figure 3 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of the multichannel time-invariant filtering oracle
% on over-determined two-source mixtures with reverberation time RT = 250
% ms as a function of the length L of the demixing filters. Each curve
% corresponds to a different number of mixture channels (plain: I = 8,
% dashed: I = 6, dash-dotted: I = 4, dotted: I = 2).

load('ir_250ms.mat');
L=[2:2:14 2*round(2.^((16:44)/4-1))];
SDR=nan(36,4,20);

% I=2
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22]; X=X(:,65:2^18+64); S=[s11;s12;s21;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,1,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% I=4
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]); s31=fftfilt(A(3,1,:),[s1,zeros(1,64)]); s41=fftfilt(A(4,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]); s32=fftfilt(A(3,2,:),[s2,zeros(1,64)]); s42=fftfilt(A(4,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22;s31+s32;s41+s42]; X=X(:,65:2^18+64); S=[s11;s12;s21;s22;s31;s32;s41;s42]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22 s31 s32 s41 s42;
    for l=1:32,
        [Se,W,SDR(l,2,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% I=6
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]); s31=fftfilt(A(3,1,:),[s1,zeros(1,64)]); s41=fftfilt(A(4,1,:),[s1,zeros(1,64)]); s51=fftfilt(A(5,1,:),[s1,zeros(1,64)]); s61=fftfilt(A(6,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]); s32=fftfilt(A(3,2,:),[s2,zeros(1,64)]); s42=fftfilt(A(4,2,:),[s2,zeros(1,64)]); s52=fftfilt(A(5,2,:),[s2,zeros(1,64)]); s62=fftfilt(A(6,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22;s31+s32;s41+s42;s51+s52;s61+s62]; X=X(:,65:2^18+64); S=[s11;s12;s21;s22;s31;s32;s41;s42;s51;s52;s61;s62]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22 s31 s32 s41 s42 s51 s52 s61 s62;
    for l=1:30,
        [Se,W,SDR(l,3,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% I=8
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]); s31=fftfilt(A(3,1,:),[s1,zeros(1,64)]); s41=fftfilt(A(4,1,:),[s1,zeros(1,64)]); s51=fftfilt(A(5,1,:),[s1,zeros(1,64)]); s61=fftfilt(A(6,1,:),[s1,zeros(1,64)]); s71=fftfilt(A(7,1,:),[s1,zeros(1,64)]); s81=fftfilt(A(8,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]); s32=fftfilt(A(3,2,:),[s2,zeros(1,64)]); s42=fftfilt(A(4,2,:),[s2,zeros(1,64)]); s52=fftfilt(A(5,2,:),[s2,zeros(1,64)]); s62=fftfilt(A(6,2,:),[s2,zeros(1,64)]); s72=fftfilt(A(7,2,:),[s2,zeros(1,64)]); s82=fftfilt(A(8,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22;s31+s32;s41+s42;s51+s52;s61+s62;s71+s72;s81+s82]; X=X(:,65:2^18+64); S=[s11;s12;s21;s22;s31;s32;s41;s42;s51;s52;s61;s62;s71;s72;s81;s82]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22 s31 s32 s41 s42 s51 s52 s61 s62 s71 s72 s81 s82;
    for l=1:28,
        [Se,W,SDR(l,4,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% Plotting figure
figure;
semilogx(L,mean(SDR(:,1,:),3),':k',L(1:32),mean(SDR(1:32,2,:),3),'-.k',L(1:30),mean(SDR(1:30,3,:),3),'--k',L(1:28),mean(SDR(1:28,4,:),3),'-k');
axis([2 2048 0 30]);
xlabel('L (taps)'); ylabel('SDR (dB)');