% BBASIS_MONOMASK3
%
% Plots figures 13 and 14 of E. Vincent, R. Gribonval and M.D. Plumbley,
% "Oracle estimators for the benchmarking of source separation algorithms,"
% Technical Report C4DM-TR-06-03, Queen Mary, University of London, 2006.
%
% Tree representation of the oracle generic WP basis for real-valued masking
% of speech mixtures. The height of each branch is proportional to the decrease in
% distortion d obtained by splitting.
%
% Tree representation of the oracle generic WP basis for real-valued masking
% of music mixtures. The height of each branch is proportional to the decrease in
% distortion d obtained by splitting.

SDR=zeros(10,4);
Dopt=18;

% Speech mixtures
x=zeros(1,2^18,10); S=zeros(2,2^18,10);
for m=1:10,
    num=['0' int2str(m+10)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x(1,:,m)=s1+s2; S(:,:,m)=[s1;s2]; clear s1 s2;
end
[Se,btree(1,:),SDR(:,1),gSDR,stree(1,:)]=bss_oracle_gbasis_monomask(x,S,0,Dopt,false);

% Music mixtures
x=zeros(1,2^18,10); S=zeros(2,2^18,10);
for m=1:10,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x(1,:,m)=s1+s2; S(:,:,m)=[s1;s2]; clear s1 s2;
end
[Se,btree(2,:),SDR(:,3),gSDR,stree(2,:)]=bss_oracle_gbasis_monomask(x,S,0,Dopt,false);

% Plotting figures
figure;
PlotBasisTree(btree(1,:),10,stree(1,:));
set(gca,'XTick',(0:11)/11.025,'XTickLabel',0:11,'YTick',[],'Box','off'); xlabel('frequency (kHz)'); title('');
figure;
PlotBasisTree(btree(2,:),10,stree(2,:));
set(gca,'XTick',(0:11)/11.025,'XTickLabel',0:11,'YTick',[],'Box','off'); xlabel('frequency (kHz)'); title('');