% MULTIMASK1
%
% Plots figure 7 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of the multichannel time-frequency masking oracles on
% two-channel three-source instantaneous mixtures as a function of MDCT
% length L. Plain: local mixing inversion with a free number of active
% sources J'm for each time-frequency point m. Dashed: J' = 2 active
% sources. Dash-dotted: J' = 1 active source. Dotted: binary masking.

L=[4 8 4*round(2.^((15:56)/4-2))];
SDR=zeros(44,4,20);
theta=[-40 +30 -5]*pi/180;
A=[sqrt(1./(1+((1-sin(theta))./(1+sin(theta))).^2));sqrt(1./(1+((1+sin(theta))./(1-sin(theta))).^2))];

% Different numbers of active sources
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).'; s3=wavread(['mix' num '_s3.wav']).';
    S(:,:,1)=A(:,1)*s1; S(:,:,2)=A(:,2)*s2; S(:,:,3)=A(:,3)*s3; X=sum(S,3); clear s1 s2 s3;
    for l=1:44,
        [Se,W,SDR(l,1,m)]=bss_oracle_pinvmask(X,S,L(l),A);
        [Se,W,SDR(l,2,m)]=bss_oracle_pinvmask(X,S,L(l),A,1);
        [Se,W,SDR(l,3,m)]=bss_oracle_pinvmask(X,S,L(l),A,2);
        [Se,W,SDR(l,4,m)]=bss_oracle_binmask(X,S,L(l));
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'-k',L,SDR(:,2),'-.k',L,SDR(:,3),'--k',L,SDR(:,4),':k');
axis([4 16384 0 27.5]);
xlabel('L (samples)'); ylabel('SDR (dB)');