% MULTIFILT4
%
% Plots figure 4 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of various source image demixing filters on
% determined two-source mixtures with reverberation time RT = 250 ms.
% Plain: time-domain oracle demixing filters of length L. Dashed:
% near-optimal frequency- domain demixing filters with L frequency bins.

load('ir_250ms.mat');
L=[4 8 4*round(2.^((15:56)/4-2))];
SDR=nan(44,2,20);

% Time-domain filters
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22]; X=X(:,65:2^18+64); S=[s11;s12;s21;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:32,
        [Se,W,SDR(l,1,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% Frequency-domain filters
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12;s21+s22]; X=X(:,65:2^18+64); S=[s11;s12;s21;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:44,
        [Se,W,SDR(l,2,m)]=bss_nearopt_multifilt(X,S,L(l));
    end
end

% Plotting figure
figure;
semilogx(L(1:32),mean(SDR(1:32,1,:),3),'-k',L,mean(SDR(:,2,:),3),'--k');
axis([4 16384 0 35]);
xlabel('L (taps)'); ylabel('SDR (dB)');