% ROBUST2
%
% Plots figure 10 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of near-optimal local mixing inversion with a free
% number of active sources J'(n,f) for each (n,f) on determined two-source mixtures
% with reverberation time RT = 250 ms as a function of the STFT length L. Each
% curve corresponds to a different hypothesized direction of source 1 for which the oracle
% activity patterns were learnt (plain: true direction, dashed: 2 deg error, dash-dotted:
% 4 deg error, dotted: 8 deg error).

T=5506;
L=[4 8 4*round(2.^((15:56)/4-2))];
SDR=zeros(44,4,20);

for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    % 2 deg error
    load('ir_move1_250ms.mat'); A_move1=A(1:2,1:2,:);
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X_move1=[s11+s12; s21+s22]; X_move1=X_move1(:,65:2^18+64); S_move1=zeros(2,2^18+64,2); S_move1(:,:,1)=[s11;s21]; S_move1(:,:,2)=[s12;s22]; S_move1=S_move1(:,65:2^18+64,:);
    % 4 deg error
    load('ir_move2_250ms.mat'); A_move2=A(1:2,1:2,:);
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X_move2=[s11+s12; s21+s22]; X_move2=X_move2(:,65:2^18+64); S_move2=zeros(2,2^18+64,2); S_move2(:,:,1)=[s11;s21]; S_move2(:,:,2)=[s12;s22]; S_move2=S_move2(:,65:2^18+64,:);
    % 8 deg error
    load('ir_move3_250ms.mat'); A_move3=A(1:2,1:2,:);
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X_move3=[s11+s12; s21+s22]; X_move3=X_move3(:,65:2^18+64); S_move3=zeros(2,2^18+64,2); S_move3(:,:,1)=[s11;s21]; S_move3(:,:,2)=[s12;s22]; S_move3=S_move3(:,65:2^18+64,:);
    % True source direction
    load('ir_250ms.mat'); A=A(1:2,1:2,:);
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X=[s11+s12; s21+s22]; X=X(:,65:2^18+64); S=zeros(2,2^18+64,2); S(:,:,1)=[s11;s21]; S(:,:,2)=[s12;s22]; S=S(:,65:2^18+64,:);
    clear s1 s2 s11 s21 s12 s22;
    for l=1:44,
        N=ceil(2^18/L(l)*2);
        D=exp(2*i*pi*64/L(l)*(0:L(l)-1)).';
        Af=sum(reshape(cat(3,A,zeros(2,2,ceil(T/L(l))*L(l)-T)),2,2,L(l),ceil(T/L(l))),4);
        Af=permute(fft(permute(Af,[3 1 2])).*repmat(D,[1 2 2]),[2 3 1]); Af=Af(:,:,1:L(l)/2+1);
        [Se,W,SDR(l,1,m)]=bss_nearopt_pinvmask(X,S,Af);
        Af=sum(reshape(cat(3,A_move1,zeros(2,2,ceil(T/L(l))*L(l)-T)),2,2,L(l),ceil(T/L(l))),4);
        Af=permute(fft(permute(Af,[3 1 2])).*repmat(D,[1 2 2]),[2 3 1]); Af=Af(:,:,1:L(l)/2+1);
        [Se,W]=bss_nearopt_pinvmask(X_move1,S_move1,Af);
        Se=apply_pinvmask_conv(X,Af,W);
        SDR(l,2,m)=sdr(reshape(Se,N*L(l),2),reshape(cat(2,S,zeros(2,N*L(l)/2-2^18,2)),N*L(l),2));
        Af=sum(reshape(cat(3,A_move2,zeros(2,2,ceil(T/L(l))*L(l)-T)),2,2,L(l),ceil(T/L(l))),4);
        Af=permute(fft(permute(Af,[3 1 2])).*repmat(D,[1 2 2]),[2 3 1]); Af=Af(:,:,1:L(l)/2+1);
        [Se,W]=bss_nearopt_pinvmask(X_move2,S_move2,Af);
        Se=apply_pinvmask_conv(X,Af,W);
        SDR(l,3,m)=sdr(reshape(Se,N*L(l),2),reshape(cat(2,S,zeros(2,N*L(l)/2-2^18,2)),N*L(l),2));
        Af=sum(reshape(cat(3,A_move3,zeros(2,2,ceil(T/L(l))*L(l)-T)),2,2,L(l),ceil(T/L(l))),4);
        Af=permute(fft(permute(Af,[3 1 2])).*repmat(D,[1 2 2]),[2 3 1]); Af=Af(:,:,1:L(l)/2+1);
        [Se,W]=bss_nearopt_pinvmask(X_move3,S_move3,Af);
        Se=apply_pinvmask_conv(X,Af,W);
        SDR(l,4,m)=sdr(reshape(Se,N*L(l),2),reshape(cat(2,S,zeros(2,N*L(l)/2-2^18,2)),N*L(l),2));
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'-k',L,SDR(:,2),'--k',L,SDR(:,3),'-.k',L,SDR(:,4),':k');
axis([4 16384 0 37.5]);
xlabel('L (samples)'); ylabel('SDR (dB)');