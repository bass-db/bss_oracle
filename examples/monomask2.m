% MONOMASK2
%
% Plots figure 7 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms,"
% Technical Report C4DM-TR-06-03, Queen Mary, University of London, 2006.
%
% Average performance of the real-valued time-frequency masking oracles on
% single-channel three-source mixtures as a function of the length L of the
% MDCT basis elements. Plain: unconstrained masks. Dashed: masks verifying
% a unitary sum constraint.

L=[4 8 4*round(2.^((15:56)/4-2))];
SDR=zeros(44,2,20);

% Unconstrained and constrained masks
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).'; s3=wavread(['mix' num '_s3.wav']).';
    x=s1+s2+s3; S=[s1;s2;s3]; clear s1 s2 s3;
    for l=1:44,
        [Se,W,SDR(l,1,m)]=bss_oracle_monomask(x,S,L(l),true,false);
        [Se,W,SDR(l,2,m)]=bss_oracle_monomask(x,S,L(l));
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'-k',L,SDR(:,2),'--k');
axis([4 16384 8 16]);
xlabel('L (samples)'); ylabel('SDR (dB)');