% MONOMASK3
%
% Plots figure 6 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of the constrained binary MDCT time-frequency masking
% oracle and the near-optimal STFT time-frequency masks on single-channel
% two-source mixtures as a function of the length L of the MDCT/STFT
% elements. Each curve corresponds to a different over-completeness factor
% (plain: eight-times overcomplete STFT, dashed: four-times overcomplete
% STFT, dash-dotted: twice overcomplete STFT, dotted: MDCT).

L=[16 32 16*round(2.^((23:56)/4-4))];
SDR=zeros(36,4,20);

% Degree of over-completeness from 1 to 8
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x=s1+s2; S=[s1;s2]; clear s1 s2;
    for l=1:36,
        [Se,W,SDR(l,1,m)]=bss_oracle_monomask(x,S,L(l),false);
        [Se,W,SDR(l,2,m)]=bss_nearopt_monomask(x,S,L(l),L(l)/2,false);
        [Se,W,SDR(l,3,m)]=bss_nearopt_monomask(x,S,L(l),L(l)/4,false);
        [Se,W,SDR(l,4,m)]=bss_nearopt_monomask(x,S,L(l),L(l)/8,false);
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),':k',L,SDR(:,2),'-.k',L,SDR(:,3),'--k',L,SDR(:,4),'-k');
axis([16 16384 8 16]);
xlabel('L (samples)'); ylabel('SDR (dB)');