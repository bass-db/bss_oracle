% ROBUST1
%
% Plots figure 9 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of the multichannel time-invariant filtering oracle
% on determined two-source mixtures with reverberation time RT = 250 ms as
% a function of the length L of the demixing filters. Each curve
% corresponds to a different hypothesized direction of source 1 for which
% the oracle demixing filters were learnt (plain: true direction, dashed: 2
% deg error, dash-dotted: 4 deg error, dotted: 8 deg error).

L=[2:2:14 2*round(2.^((16:44)/4-1))];
SDR=zeros(36,4,20);

for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    % 2 deg error
    load('ir_move1_250ms.mat');
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X_move1=[s11+s12; s21+s22]; X_move1=X_move1(:,65:2^18+64); S_move1=[s11;s21;s12;s22]; S_move1=S_move1(:,65:2^18+64);
    % 4 deg error
    load('ir_move2_250ms.mat');
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X_move2=[s11+s12; s21+s22]; X_move2=X_move2(:,65:2^18+64); S_move2=[s11;s21;s12;s22]; S_move2=S_move2(:,65:2^18+64);
    % 8 deg error
    load('ir_move3_250ms.mat');
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X_move3=[s11+s12; s21+s22]; X_move3=X_move3(:,65:2^18+64); S_move3=[s11;s21;s12;s22]; S_move3=S_move3(:,65:2^18+64);
    % True source direction
    load('ir_250ms.mat');
    s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);    
    X=[s11+s12; s21+s22]; X=X(:,65:2^18+64); S=[s11;s21;s12;s22]; S=S(:,65:2^18+64);
    clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,1,m)]=bss_oracle_multifilt(X,S,L(l));
        [Se,W]=bss_oracle_multifilt(X_move1,S_move1,L(l));
        Se=apply_multifilt_temp(X,W);
        SDR(l,2,m)=sdr(Se,[zeros(4,L(l)/2-1),S,zeros(4,L(l)/2)]);
        [Se,W]=bss_oracle_multifilt(X_move2,S_move2,L(l));
        Se=apply_multifilt_temp(X,W);
        SDR(l,3,m)=sdr(Se,[zeros(4,L(l)/2-1),S,zeros(4,L(l)/2)]);
        [Se,W]=bss_oracle_multifilt(X_move3,S_move3,L(l));
        Se=apply_multifilt_temp(X,W);
        SDR(l,4,m)=sdr(Se,[zeros(4,L(l)/2-1),S,zeros(4,L(l)/2)]);
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'-k',L,SDR(:,2),'--k',L,SDR(:,3),'-.k',L,SDR(:,4),':k');
axis([2 2048 0 25]);
xlabel('L (taps)'); ylabel('SDR (dB)');