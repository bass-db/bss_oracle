% MULTIFILT1
%
% Plots figure 2 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms," Signal
% Processing 87(8), p. 1933-1950, 2007.
%
% Average performance of the multichannel time-invariant filtering oracle
% on determined two-source mixtures as a function of the length L of the
% demixing filters. Each curve corresponds to a different reverberation
% time (plain: anechoic, dashed: RT = 50 ms, dash-dotted: RT = 250 ms,
% dotted: RT = 1.25 s).

L=[2:2:14 2*round(2.^((16:44)/4-1))];
SDR=zeros(36,4,20);

% Anechoic
load('ir_anechoic.mat');
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12; s21+s22]; X=X(:,65:2^18+64); S=[s11;s21;s12;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,1,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% RT=50 ms
load('ir_50ms.mat');
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12; s21+s22]; X=X(:,65:2^18+64); S=[s11;s21;s12;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,2,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% RT=250 ms
load('ir_250ms.mat');
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12; s21+s22]; X=X(:,65:2^18+64); S=[s11;s21;s12;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,3,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% RT=1.25 s
load('ir_1.25s.mat');
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s11=fftfilt(A(1,1,:),[s1,zeros(1,64)]); s21=fftfilt(A(2,1,:),[s1,zeros(1,64)]);
    s2=wavread(['mix' num '_s2.wav']).'; s12=fftfilt(A(1,2,:),[s2,zeros(1,64)]); s22=fftfilt(A(2,2,:),[s2,zeros(1,64)]);
    X=[s11+s12; s21+s22]; X=X(:,65:2^18+64); S=[s11;s21;s12;s22]; S=S(:,65:2^18+64); clear s1 s2 s11 s21 s12 s22;
    for l=1:36,
        [Se,W,SDR(l,4,m)]=bss_oracle_multifilt(X,S,L(l));
    end
end

% Plotting figure
SDR=mean(SDR,3);
figure;
semilogx(L,SDR(:,1),'-k',L,SDR(:,2),'--k',L,SDR(:,3),'-.k',L,SDR(:,4),':k');
axis([2 2048 0 50]);
xlabel('L (taps)'); ylabel('SDR (dB)');