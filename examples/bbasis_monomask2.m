% BBASIS_MONOMASK2
%
% Plots figure 12 of E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle
% estimators for the benchmarking of source separation algorithms,"
% Technical Report C4DM-TR-06-03, Queen Mary, University of London, 2006.
%
% Average performance of the constrained best WP basis masking oracle
% on single-channel two-source mixtures. Left: SDR as a function of the maximal
% packet depth Dmax with minimal depth Dmin = 0. Right: SDR as a function of
% the minimal packet depth Dmin with maximal depth Dmax = 18. Plain: real-valued
% masking. Dashed: binary masking.

% Performance as a function of Dmax (Dmin = 0)
Da=0:18;
SDRa=zeros(19,2,20);
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x=s1+s2; S=[s1;s2]; clear s1 s2;
    for d=0:18,
        [Se,btree,W,SDRa(d+1,1,m)]=bss_oracle_bbasis_monomask(x,S,0,d,false);
        [Se,btree,W,SDRa(d+1,2,m)]=bss_oracle_bbasis_monomask(x,S,0,d,false,false);
    end
end
[val,ind]=max(mean(SDRa,3));
Dopt=ind(1)-1;

% Performance as a function of Dmin (Dmax = Dopt)
Db=0:Dopt;
SDRb=zeros(Dopt+1,2,20);
for m=1:20,
    num=['0' int2str(m)]; num=num(end-1:end);
    s1=wavread(['mix' num '_s1.wav']).'; s2=wavread(['mix' num '_s2.wav']).';
    x=s1+s2; S=[s1;s2]; clear s1 s2;
    for d=0:Dopt,
        [Se,btree,W,SDRb(d+1,1,m)]=bss_oracle_bbasis_monomask(x,S,d,Dopt,false);
        [Se,btree,W,SDRb(d+1,2,m)]=bss_oracle_bbasis_monomask(x,S,d,Dopt,false,false);
    end
end

% Plotting figure
SDRa=mean(SDRa,3); SDRb=mean(SDRb,3);
figure;
plot(Da,SDRa(:,1),'-k',Da,SDRa(:,2),'--k'); axis([0 18 7 20]);
xlabel('Dmax'); ylabel('SDR (dB)');
figure;
plot(Db,SDRb(:,1),'-k',Db,SDRb(:,2),'--k'); axis([0 Dopt 7 20]);
xlabel('Dmin'); ylabel('SDR (dB)');