function [Se,W,SDR]=bss_oracle_monomask(x,S,L,mreal,mconst)

% BSS_ORACLE_MONOMASK Oracle estimator for single-channel source separation
% by time-frequency masking using MDCT with sine window.
%
% [Se,W,SDR]=bss_oracle_monomask(x,S,L)
% [Se,W,SDR]=bss_oracle_monomask(x,S,L,mreal)
% [Se,W,SDR]=bss_oracle_monomask(x,S,L,mreal,mconst)
%
% Inputs:
% x: 1 x T vector containing the single-channel mixture signal
% S: J x T matrix containing the target signals (e.g. source images)
% L: length of the MDCT window in samples (must be a multiple of 4)
% mreal: true for real-valued masking (default)
%        false for binary masking
% mconst: true when the masks are subject to a unitary sum constraint (default)
%         false otherwise
%
% Outputs:
% Se: J x T matrix containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: L/2 x N x J table containing the oracle masks with N=ceil(T/L*2)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 5.2.
%
% See also mdct, imdct.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
if nargin<4, mreal=true; end
if nargin<5, mconst=true; end
[I,T]=size(x);
[J,Ts]=size(S);
if I>1, error('The mixture signal must be a row vector.'); end
if J>Ts, error('Target signals must be in rows.'); end
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if mconst && any(abs(x-sum(S,1)) > eps), error('When computing constrained masks, the mixture signal must be equal to the sum of the target signals.'); end
if (J==2), mconst=false; end
N=ceil(T/L*2);
if mreal && mconst, warning('BSSOracle:IntensiveComputation','Computing constrained real-valued masks for %d\ntime-frequency points and %d sources. This may take some time.',L/2*N,J); end;

%%% Computing MDCT ratio of input data %%%
X=mdct(x,L);
R=zeros(L/2,N,J);
for j=1:J,
    R(:,:,j)=mdct(S(j,:),L)./(X+realmin);
end

%%% Computing oracle masks %%%
W=zeros(L/2,N,J);
if mconst && mreal,
    %Constrained real-valued mask
    for f=1:L/2,
        for n=1:N,
            W(f,n,:)=optim_coeffs(reshape(R(f,n,:),J,1));
        end
    end
elseif mconst && ~mreal,
    %Constrained binary mask
    [val,ind]=max(R,[],3);
    W((1:L/2).'*ones(1,N)+ones(L/2,1)*L/2*(0:N-1)+(ind-1)*N*L/2)=1;
elseif ~mconst && mreal,
    %Unconstrained real-valued mask
    W=min(max(R,0),1);
else
    %Unconstrained binary mask
    W=(R >= .5);
end

%%% Deriving oracle estimates of the target signals %%%
% Applying oracle masks
Se=zeros(J,N*L/2);
for j=1:J,
    Se(j,:)=imdct(X.*W(:,:,j));
end
% SDR
SDR=sdr(Se,[S,zeros(J,N*L/2-T)]);
% Truncating target signal estimates to original target length
Se=Se(:,1:T);

return;
