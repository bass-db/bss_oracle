function [Se,btree,W,SDR,stree]=bss_oracle_bbasis_pinvmask(X,S,A,Dmin,Dmax,Ja,pcos)

% BSS_ORACLE_BBASIS_PINVMASK Oracle estimator for multichannel source
% separation of instantaneous mixtures by time-frequency masking and
% mixing matrix pseudo-inversion using the best CP/WP basis.
%
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_pinvmask(X,S,A,Dmin,Dmax)
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_pinvmask(X,S,A,Dmin,Dmax,Ja)
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_pinvmask(X,S,A,Dmin,Dmax,Ja,pcos)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: I x T x J table containing the target signals (source images)
% A: I x J real-valued mixing matrix (may be different from the one actually
%     used to generate S)
% Dmin: minimal packet depth
% Dmax: maximal packet depth
% Ja: number of active sources per time-frequency point (by default or if Ja=0,
%      the best number is estimated for each time-frequency point)
% pcos: true for CP basis with sine window (default)
%       false for WP basis with symmlet-8
%
% Outputs:
% Se: I x T x J table containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% btree: 1 x (2^(Dmax+1)-1) vector of binary values representing the tree
%        structure corresponding to the oracle best basis
% W: 2^N x (Dmax-Dmin+1) x J table containing the oracle masks for each scale
%    with N=nextpow2(T)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
% stree: 1 x (2^(Dmax+1)-1) vector containing the oracle distortion for all basis
% elements (infinite for disallowed scales)
%
% Reference:
% E. Vincent and R. Gribonval, "Blind criterion and oracle bound for
% instantaneous audio source separation using adaptive time-frequency
% representations," in Proc. IEEE Workshop on Applications of Signal
% Processing to Audio and Acoustics (WASPAA), 2007, Section 3.2.
%
% See also CPAnalysis, CPSynthesis, WPAnalysis, WPSynthesis.

%%% Errors and warnings %%%
if nargin<5, error('Not enough input arguments.'); end
if nargin<6, Ja=0; end
if nargin<7, pcos=true; end
[I,T]=size(X);
[Is,Ts,J]=size(S);
[Im,Jm]=size(A);
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if I~=Is, error('Mixture and target signals have a different number of channels.'); end
if I~=Im, error('The number of rows in the mixing matrix must be equal to the number of mixture signals.'); end
if J~=Jm, error('The number of columns in the mixing matrix must be equal to the number of sources.'); end
if Ja>I, error('The number of active sources must be smaller than the number of sources.'); end
if any(any(abs(X-sum(S,3)) > eps)), error('The mixture signal must be equal to the sum of the target signals.'); end
if Dmin>Dmax, error('The minimal depth must be smaller than the maximal depth.'); end
N=nextpow2(T);

%%% Computing input data transforms %%%
% Zero-padding
X=[X,zeros(I,2^N-T)];
S=cat(2,S,zeros(I,2^N-T,J));
XX=zeros(2^N,Dmax-Dmin+1,I);
SS=zeros(2^N,Dmax-Dmin+1,I,J);
if pcos,
    % CP transform (all scales)
    for i=1:I,
        XXX=CPAnalysis(X(i,:),Dmax);
        XX(:,:,i)=XXX(:,Dmin+1:Dmax+1);
        for j=1:J,
            YYY=CPAnalysis(S(i,:,j),Dmax);
            SS(:,:,i,j)=YYY(:,Dmin+1:Dmax+1);
        end
    end
else
    % WP transform (all scales)
    for i=1:I,
        qmf=MakeONFilter('Symmlet',8);
        XXX=WPAnalysis(X(i,:),Dmax,qmf);
        XX(:,:,i)=XXX(:,Dmin+1:Dmax+1);
        for j=1:J,
            YYY=WPAnalysis(S(i,:,j),Dmax,qmf);
            SS(:,:,i,j)=YYY(:,Dmin+1:Dmax+1);
        end
    end
end
XX=reshape(XX,2^N*(Dmax-Dmin+1),I);

%%% Computing oracle activity pattern and distortion for each element and each scale %%%
% Source activity patterns
act=zeros(2^J,J);
for p=1:J,
    act(:,p)=reshape(repmat([0 1],[2^(J-p) 2^(p-1)]),2^J,1);
end
% Distortion for each pattern
disto=zeros(2^N,Dmax-Dmin+1,2^J);
for p=1:2^J,
    ind=find(act(p,:));
    nind=length(ind);
    if (nind==Ja) || (Ja==0),
        % Pseudo-inversion of mixing matrix for active sources
        B=pinv(A(:,ind));
        SSe=zeros(2^N,Dmax-Dmin+1,I,J);
        for j=1:nind,
            SSe(:,:,:,ind(j))=reshape(XX*B(j,:).'*A(:,ind(j)).',2^N,Dmax-Dmin+1,I);
        end
        disto(:,:,p)=sum(sum((SSe-SS).^2,4),3);
    else
        % Infinite distortion for disallowed number of active sources
        disto(:,:,p)=Inf;
    end
end
% Selecting best pattern and deriving oracle target transforms
[disto,pat]=min(disto,[],3);
W=zeros(2^N*(Dmax-Dmin+1),J);
SSe=zeros(2^N*(Dmax-Dmin+1),J);
for p=1:2^J,
    ind=find(act(p,:));
    n=find(pat==p);
    W(n,ind)=1;
    B=pinv(A(:,ind));
    SSe(n,ind)=XX(n,:)*B.';
end
W=reshape(W,2^N,Dmax-Dmin+1,J);
SSe=reshape(SSe,2^N,Dmax-Dmin+1,J);
% Computing oracle distortion
stree=zeros(1,2^(Dmax+1)-1);
for d=0:Dmax,
    if d < Dmin,
        % Infinite distortion for disallowed scales
        stree(node(d,0:2^d-1))=inf;
    else
        for b=0:(2^d-1),
            % Storing total distortion
            stree(node(d,b))=sum(disto(packet(d,b,2^N),d-Dmin+1));
        end
    end
end

%%% Selecting best basis %%%
global WLVERBOSE;
WLVERBOSE='No';
btree=BestBasis(stree,Dmax);

%%% Deriving oracle estimates of the target signals %%%
Se=zeros(I,2^N,J);
for j=1:J,
    YYY=[zeros(2^N,Dmin),SSe(:,:,j)];
    if pcos,
        % Inverse CP transform (using best basis only)
        Se(:,:,j)=A(:,j)*CPSynthesis(btree,YYY);
    else
        % Inverse WP transform (using best basis only)
        Se(:,:,j)=A(:,j)*WPSynthesis(btree,YYY,qmf);
    end
end
% SDR
SDR=sdr(reshape(Se,I*2^N,J),reshape(S,I*2^N,J));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;