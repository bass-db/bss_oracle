function Se=apply_multifilt_freq(X,W,M)

% APPLY_MULTIFILT_FREQ Apply frequency-domain demixing matrices using STFT
% with sine window.
%
% Se=apply_multifilt_freq(X,W)
% Se=apply_multifilt_freq(X,W,M)
%
% Inputs:
% X: I x T matrix containing the mixture signals
% W: J x I x (L/2+1) table containing complex demixing matrices for positive
%      frequencies, with L being the length of the STFT window in samples
% M: step between successive windows in samples (must be even, a divider of
%    L and smaller than L/2) (default: L/2)
%
% Output:
% Se: J x (N*M) matrix containing the demixed signals with N=ceil(T/M)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 4.4.
%
% See also stft, istft.

%%% Errors and warnings %%%
if nargin<2, error('Not enough input arguments.'); end
[I,T]=size(X);
[J,Iw,L]=size(W);
if I>T, error('Mixture signals must be in rows.'); end
if I~=Iw, error('The number of colums of the demixing matrices must be equal to the number of mixture signals.'); end
if L==2*floor(L/2), error('The number of positive frequency channels must be odd.'); end
L=2*(L-1);
if nargin<3, M=L/2; end
N=ceil(T/M);

%%% Applying demixing matrices %%%
% STFT of input data
Xf=zeros(L/2+1,N,I);
for i=1:I,
    Xf(:,:,i)=stft(X(i,:),L,M);
end
% STFT of demixed signals
Sef=zeros(L/2+1,N,J);
for f=1:L/2+1,
    Sef(f,:,:)=reshape(Xf(f,:,:),N,I)*W(:,:,f).';
end
% ISTFT
Se=zeros(J,N*M);
for j=1:J,
    Se(j,:)=istft(Sef(:,:,j),M);
end

return;
