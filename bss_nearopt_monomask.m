function [Se,W,SDR]=bss_nearopt_monomask(x,S,L,M,mreal,mconst)

% BSS_NEAROPT_MONOMASK Near-optimal time-frequency masks for 
% single-channel source separation using STFT with sine window
% (coefficients derived for each time-frequency point separately).
%
% [Se,W,SDR]=bss_nearopt_monomask(x,S,L)
% [Se,W,SDR]=bss_nearopt_monomask(x,S,L,M)
% [Se,W,SDR]=bss_nearopt_monomask(x,S,L,M,mreal)
% [Se,W,SDR]=bss_nearopt_monomask(x,S,L,M,mreal,mconst)
%
% Inputs:
% x: 1 x T vector containing the single-channel mixture signal
% S: J x T matrix containing the target signals (e.g. source images)
% L: length of the STFT window in samples (must be a multiple of 4)
% M: step between successive windows in samples (must be even, a divider of
%    L and smaller than L/2) (default: L/2)
% mreal: true for real-valued masking (default)
%        false for binary masking
% mconst: true when the masks are subject to a unitary sum constraint (default)
%         false otherwise
%
% Outputs:
% Se: J x T matrix containing the near-optimal estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: (L/2+1) x N x J table containing the near-optimal masks with N=ceil(T/M)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 5.4.
%
% See also stft, istft.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
if nargin<4, M=L/2; end
if nargin<5, mreal=true; end
if nargin<6, mconst=true; end
[I,T]=size(x);
[J,Ts]=size(S);
if I>1, error('The mixture signal must be a row vector.'); end
if J>Ts, error('Target signals must be in rows.'); end
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if mconst && any(abs(x-sum(S,1)) > eps), error('When computing constrained masks, the mixture signal must be equal to the sum of the target signals.'); end
if (J==2), mconst=false; end
N=ceil(T/M);
if mreal && mconst, warning('BSSOracle:IntensiveComputation','Computing constrained real-valued masks for %d\nbasis elements and %d sources. This may take some time.',(Dmax-Dmin+1)*(2^N)*K,J); end;

%%% Computing real part of STFT ratio of input data %%%
X=stft(x,L,M);
R=zeros(L/2+1,N,J);
for j=1:J,
    R(:,:,j)=real(stft(S(j,:),L,M)./(X+realmin));
end

%%% Computing near-optimal masks %%%
W=zeros(L/2+1,N,J);
if mconst && mreal,
    % Constrained real-valued mask
    for f=1:L/2+1,
        for n=1:N,
            W(f,n,:)=optim_coeffs(reshape(R(f,n,:),J,1));
        end
    end
elseif mconst && ~mreal,
    % Constrained binary mask
    [val,ind]=max(R,[],3);
    W((1:L/2+1).'*ones(1,N)+ones(L/2+1,1)*(L/2+1)*(0:N-1)+(ind-1)*N*(L/2+1))=1;
elseif ~mconst && mreal,
    % Unconstrained real-valued mask
    W=min(max(R,0),1);
else
    % Unconstrained binary mask
    W=(R >= .5);
end

%%% Deriving near-optimal estimates of the target signals %%%
% Applying near-optimal masks
Se=zeros(J,N*M);
for j=1:J,
    Se(j,:)=istft(X.*W(:,:,j),M);
end
% SDR
SDR=sdr(Se,[S,zeros(J,N*M-T)]);
% Truncating target signal estimates to original target length
Se=Se(:,1:T);

return;
