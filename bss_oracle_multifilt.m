function [Se,W,SDR]=bss_oracle_multifilt(X,S,L)

% BSS_ORACLE_MULTIFILT Oracle estimator for source separation by
% multichannel time-invariant filtering in the time domain.
%
% [Se,W,SDR]=bss_oracle_multifilt(X,S,L)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% S: J x T matrix containing the target signals (e.g. sources or source images)
% L: length of the demixing filters in samples
%
% Outputs:
% Se: J x T matrix containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% W: J x I x L table containing the coefficients of the oracle demixing filters
%    (delays from -L/2+1 to L/2)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 4.2.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
[I,T]=size(X);
[J,Ts]=size(S);
if I>T, error('Mixture signals must be in rows.'); end
if J>Ts, error('Target signals must be in rows.'); end
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if L~=2*floor(L/2), error('The length of the demixing filters must be even.'); end
if I*L >= 1024, warning('BSSOracle:IntensiveComputation','Computing %d filter coefficients for %d targets. This may take some time.',I*L,J); end;

%%% Computing coefficients of least squares problem via FFT %%%
% Zero padding and FFT of input data
X=[X,zeros(I,L-1)];
S=[zeros(J,L/2-1),S,zeros(J,L/2)];
N=2^nextpow2(T+L-1);
Xf=fft(X,N,2);
Sf=fft(S,N,2);
% Inner products between delayed mixture channels
G=zeros(I*L);
for i1=0:I-1,
    for i2=0:i1,
        XXf=Xf(i1+1,:).*conj(Xf(i2+1,:));
        XXf=real(ifft(XXf));
        XX=toeplitz(XXf([1 N:-1:N-L+2]),XXf(1:L));
        G(i1*L+1:i1*L+L,i2*L+1:i2*L+L)=XX;
        G(i2*L+1:i2*L+L,i1*L+1:i1*L+L)=XX.';
    end
end
% Inner products between delayed mixture channels and targets
D=zeros(I*L,J);
for i=0:I-1,
    XSf=ones(J,1)*Xf(i+1,:).*conj(Sf);
    XSf=real(ifft(XSf,[],2));
    D(i*L+1:i*L+L,:)=XSf(:,[1 N:-1:N-L+2]).';
end

%%% Computing oracle demixing filters %%%
W=inv(G)*D;
W=permute(reshape(W,L,I,J),[3 2 1]);

%%% Deriving oracle estimates of the target signals %%%
% Applying oracle demixing filters
Se=apply_multifilt_temp(X(:,1:T),W);
% SDR
SDR=sdr(Se,S);
% Truncating target signal estimates to original target length
Se=Se(:,L/2:L/2+T-1);

return;
