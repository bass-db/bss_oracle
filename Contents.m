% BSS Oracle Toolbox for the computation of oracle source separation estimators.
% Version 2.1 - October 12, 2007
% 
% Oracle source estimators
%  bss_oracle_multifilt.m       - Oracle time-domain multichannel time-invariant filtering.
%  bss_oracle_monomask.m        - Oracle single-channel masking using MDCT.
%  bss_oracle_binmask.m         - Oracle multichannel binary masking using MDCT.
%  bss_oracle_pinvmask.m        - Oracle multichannel masking with mixing matrix pseudo-inversion using MDCT.
%  bss_oracle_bbasis_monomask.m - Oracle single-channel masking using the best adaptive CP/WP basis.
%  bss_oracle_bbasis_binmask.m  - Oracle multichannel binary masking using the best adaptive CP/WP basis.
%  bss_oracle_bbasis_pinvmask.m - Oracle multichannel masking with mixing matrix pseudo-inversion using the best adaptive CP/WP basis.
%  bss_oracle_gbasis_monomask.m - Oracle single-channel masking using the best generic CP/WP basis.
%
% Near-optimal source estimators
%  bss_nearopt_multifilt.m      - Oracle frequency-domain multichannel time-invariant filtering.
%  bss_nearopt_monomask.m       - Oracle single-channel masking using STFT.
%  bss_nearopt_binmask.m        - Oracle multichannel binary masking using STFT.
%  bss_nearopt_pinvmask.m       - Oracle multichannel masking with mixing matrix pseudo-inversion using STFT.
%
% Time-frequency transforms
%  mdct.m                       - Modified Discrete Cosine Transform.
%  imdct.m                      - Inverse Modified Discrete Cosine Transform.
%  stft.m                       - Short-Term Fourier Transform.
%  istft.m                      - Inverse Short-Term Fourier Transform.
%
% Filtering and masking functions
%  apply_multifilt_temp.m       - Apply time-domain multichannel time-invariant filters.
%  apply_multifilt_freq.m       - Apply frequency-domain multichannel time-invariant filters.
%  apply_pinvmask_inst.m        - Apply multichannel masks with mixing matrix pseudo-inversion using MDCT.
%  apply_pinvmask_conv.m        - Apply multichannel masks with mixing matrix pseudo-inversion using STFT.
%
% Auxiliary functions
%  optim_coeffs.m               - Oracle constrained real-valued masking coefficients for a single basis element.
%  pinv_filt.m                  - Pseudo-inversion of a filter system.
%  sdr.m                        - Signal to Distortion Ratio.
