function [Se,btree,W,SDR,stree]=bss_oracle_bbasis_monomask(x,S,Dmin,Dmax,pcos,mreal,mconst)

% BSS_ORACLE_BBASIS_MONOMASK Oracle estimator for single-channel source separation
% by time-frequency masking using the best CP/WP basis.
%
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_monomask(x,S,Dmin,Dmax)
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_monomask(x,S,Dmin,Dmax,pcos)
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_monomask(x,S,Dmin,Dmax,pcos,mreal)
% [Se,btree,W,SDR,stree]=bss_oracle_bbasis_monomask(x,S,Dmin,Dmax,pcos,mreal,mconst)
%
% Inputs:
% x: 1 x T vector containing the single-channel mixture signal
% S: J x T matrix containing the target signals (e.g. source images)
% Dmin: minimal packet depth
% Dmax: maximal packet depth
% pcos: true for CP basis with sine window (default)
%       false for WP basis with symmlet-8
% mreal: true for real-valued masking (default)
%        false for binary masking
% mconst: true when the masks are subject to a unitary sum constraint (default)
%         false otherwise
%
% Outputs:
% Se: J x T matrix containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% btree: 1 x (2^(Dmax+1)-1) vector of binary values representing the tree
%        structure corresponding to the oracle best basis
% W: 2^N x (Dmax-Dmin+1) x J table containing the oracle masking coefficients
%    for each scale with N=nextpow2(T)
% SDR: achieved SDR in deciBels (before truncation of the target estimates)
% stree: 1 x (2^(Dmax+1)-1) vector containing the oracle distortion for all basis
% elements (infinite for disallowed scales)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Technical Report
% C4DM-TR-06-03, Queen Mary, University of London, 2006, Section 7.2.
%
% See also CPAnalysis, CPSynthesis, WPAnalysis, WPSynthesis.

%%% Errors and warnings %%%
if nargin<4, error('Not enough input arguments.'); end
if nargin<5, pcos=true; end
if nargin<6, mreal=true; end
if nargin<7, mconst=true; end
[I,T]=size(x);
[J,Ts]=size(S);
if I>1, error('The mixture signal must be a row vector.'); end
if J>Ts, error('Target signals must be in rows.'); end
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if mconst && any(abs(x-sum(S,1)) > eps), error('When computing constrained masks, the mixture signal must be equal to the sum of the target signals.'); end
if (J==2), mconst=false; end
if Dmin>Dmax, error('The minimal depth must be smaller than the maximal depth.'); end
N=nextpow2(T);
if mreal && mconst, warning('BSSOracle:IntensiveComputation','Computing constrained real-valued masks for %d\nbasis elements and %d sources. This may take some time.',(Dmax-Dmin+1)*(2^N),J); end;

%%% Computing input data transforms %%%
% Zero-padding
x=[x,zeros(1,2^N-T)];
S=[S,zeros(J,2^N-T)];
SS=zeros(2^N,Dmax-Dmin+1,J);
if pcos,
    % CP transform (all scales)
    X=CPAnalysis(x,Dmax);
    X=X(:,Dmin+1:Dmax+1);
    for j=1:J,
        Y=CPAnalysis(S(j,:),Dmax);
        SS(:,:,j)=Y(:,Dmin+1:Dmax+1);
    end
else
    % WP transform (all scales)
    qmf=MakeONFilter('Symmlet',8);
    X=WPAnalysis(x,Dmax,qmf);
    X=X(:,Dmin+1:Dmax+1);
    for j=1:J,
        Y=WPAnalysis(S(j,:),Dmax,qmf);
        SS(:,:,j)=Y(:,Dmin+1:Dmax+1);
    end
end

%%% Computing oracle mask and distortion for each element and each scale %%%
W=zeros(2^N,Dmax-Dmin+1,J);
stree=zeros(1,2^(Dmax+1)-1);
for d=0:Dmax,
    if d < Dmin,
        % Infinite distortion for disallowed scales
        stree(node(d,0:2^d-1))=inf;
    else
        for b=0:(2^d-1),
            Xp=X(packet(d,b,2^N),d-Dmin+1).';
            Sp=reshape(SS(packet(d,b,2^N),d-Dmin+1,:),2^(N-d),J).';
            % Ratio of target and mixture coefficients
            Rp=Sp./(ones(J,1)*(Xp+realmin));
            Wp=zeros(J,2^(N-d));
            if mconst && mreal,
                % Constrained real-valued mask
                for t=1:2^(N-d),
                    if any(Rp(:,t)),
                        Wp(:,t)=optim_coeffs(Rp(:,t));
                    else
                        Wp(:,t)=1/J;
                    end
                end
            elseif mconst && ~mreal,
                % Constrained binary mask
                [val,ind]=max(Rp,[],1);
                Wp(J*(0:2^(N-d)-1)+ind)=1;
            elseif ~mconst && mreal,
                % Unconstrained real-valued mask
                Wp=min(max(Rp,0),1);
            else
                % Unconstrained binary mask
                Wp=(Rp >= .5);
            end
            % Computing distortion and storing oracle masks
            W(packet(d,b,2^N),d-Dmin+1,:)=Wp.';
            stree(node(d,b))=sum(sum((Sp-Wp.*(ones(J,1)*Xp)).^2));
        end
    end
end

%%% Selecting best basis %%%
global WLVERBOSE;
WLVERBOSE='No';
btree=BestBasis(stree,Dmax);

%%% Deriving oracle estimates of the target signals %%%
Se=zeros(J,2^N);
for j=1:J,
    % Applying oracle mask
    Y=[zeros(2^N,Dmin),X.*W(:,:,j)];
    if pcos,
        % Inverse CP transform (using best basis only)
        Se(j,:)=CPSynthesis(btree,Y);
    else
        % Inverse WP transform (using best basis only)
        Se(j,:)=WPSynthesis(btree,Y,qmf);
    end
end
% SDR
SDR=sdr(Se,S);
% Truncating target signal estimates to original target length
Se=Se(:,1:T);

return;
