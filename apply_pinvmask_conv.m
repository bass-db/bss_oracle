function Se=apply_pinvmask_conv(X,A,W,M)

% APPLY_PINVMASK_CONV Apply multichannel time-frequency masks
% with mixing matrix pseudo-inversion using STFT with sine window.
%
% Se=apply_pinvmask_conv(X,A,W)
% Se=apply_pinvmask_conv(X,A,W,M)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% A: I x J x (L/2+1) table containing complex mixing matrices for positive
%     frequencies, with L being the length of the STFT window in samples
% W: (L/2+1) x N x J table of binary coefficients indicating the source
%      activity patterns with N=ceil(T/M)
% M: step between successive windows in samples (must be even, a divider of
%    L and smaller than L/2) (default: L/2)
%
% Output:
% Se: I x (N*M) x J table containing the derived source images
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 6.4.
%
% See also stft, istft.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
[I,T]=size(X);
[Im,J,L]=size(A);
[Lw,Nw,Jw]=size(W);
if L==2*floor(L/2), error('The number of positive frequency channels must be odd.'); end
if L~=Lw, error('The number of frequency channels in the mixing matrices and the masks must be equal.'); end
L=2*(L-1);
if nargin<4, M=L/2; end
if I~=Im, error('The number of rows in the mixing matrices must be equal to the number of mixture signals.'); end
if J~=Jw, error('The number of sources in the mixing matrices and the masks must be equal.'); end
N=ceil(T/M);
if N~=Nw, error('The step between successive windows must be consistent with the number of time frames of the masks.'); end

%%% Applying multichannel masks %%%
% STFT of input data
XX=zeros(L/2+1,N,I);
for i=1:I,
    XX(:,:,i)=stft(X(i,:),L,M);
end
% Source activity patterns
act=zeros(2^J,J);
for p=1:J,
    act(:,p)=reshape(repmat([0 1],[2^(J-p) 2^(p-1)]),2^J,1);
end
% STFT of source images
SSe=zeros(L/2+1,N,I,J);
for p=1:2^J,
    ind=find(act(p,:));
    nind=length(ind);
    for f=1:L/2+1,
        n=find(all(reshape(W(f,:,:),N,J)==ones(N,1)*act(p,:),2));
        B=pinv(A(:,ind,f));
        for j=1:nind,
            SSe(f,n,:,ind(j))=reshape(reshape(XX(f,n,:),length(n),I)*B(j,:).'*A(:,ind(j),f).',1,length(n),I);
        end
    end
end
% Source images
Se=zeros(I,N*M,J);
for i=1:I,
    for j=1:J,
        Se(i,:,j)=istft(SSe(:,:,i,j),M);
    end
end

return;
