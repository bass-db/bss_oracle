function Se=apply_pinvmask_inst(X,A,W)

% APPLY_PINVMASK_INST Apply multichannel time-frequency masks
% with mixing matrix pseudo-inversion using MDCT with sine window.
%
% Se=apply_pinvmask_inst(X,A,W)
%
% Inputs:
% X: I x T matrix containing the multichannel mixture signal
% A: I x J real-valued mixing matrix
% W: L/2 x N x J table of binary coefficients indicating the source activity patterns 
%      with L being the length of the MDCT window in samples and N=ceil(T/M)
%
% Output:
% Se: I x (N*L/2) x J table containing the derived source images
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Signal Processing 87(8),
% p. 1933-1950, 2007, Section 6.1.2.
%
% See also mdct, imdct.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
[I,T]=size(X);
[Im,J]=size(A);
[L,Nw,Jw]=size(W);
if I~=Im, error('The number of rows of the mixing matrix must be equal to the number of mixture signals.'); end
if J~=Jw, error('The number of sources in the mixing matrix and the masks must be equal.'); end
L=L*2;
N=ceil(T/L*2);
if N~=Nw, error('The number of time frames of the masks must be consistent with the length of the mixture signals.'); end

%%% Applying multichannel masks %%%
% MDCT of input data
XX=zeros(L/2,N,I);
for i=1:I,
    XX(:,:,i)=mdct(X(i,:),L);
end
% Source activity patterns
act=zeros(2^J,J);
for p=1:J,
    act(:,p)=reshape(repmat([0 1],[2^(J-p) 2^(p-1)]),2^J,1);
end
% MDCT of source images
SSe=zeros(L/2,N,I,J);
for p=1:2^J,
    ind=find(act(p,:));
    nind=length(ind);
    for f=1:L/2,
        n=find(all(reshape(W(f,:,:),N,J)==ones(N,1)*act(p,:),2));
        B=pinv(A(:,ind));
        for j=1:nind,
            SSe(f,n,:,ind(j))=reshape(reshape(XX(f,n,:),length(n),I)*B(j,:).'*A(:,ind(j)).',1,length(n),I);
        end
    end
end
% Source images
Se=zeros(I,N*L/2,J);
for i=1:I,
    for j=1:J,
        Se(i,:,j)=imdct(SSe(:,:,i,j));
    end
end

return;
