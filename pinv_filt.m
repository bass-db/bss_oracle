function [W,B,SIR]=pinv_filt(A,zdel,L)

% PINV_FILT Pseudo-inversion of a filter system.
%
% [W,B,SIR]=pinv_filt(A,zdel,L)
%
% Inputs:
% A: I x J x T table containing filters of length T (delays from -zdel+1 to T-zdel)
% zdel: sample index corresponding to zero delay
% L: length of the pseudo-inverse filters in samples
%
% Outputs:
% W: J x I x L table containing pseudo-inverse filters (delays from -L/2+1 to L/2)
% B: J x J x (T+L-1) product of W and A (delays from -zdel-L/2+2 to T-zdel+L/2)
% SIR: achieved SIR in deciBels
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Technical Report
% C4DM-TR-06-03, Queen Mary, University of London, 2006, Section 4.3.3.

%%% Errors and warnings %%%
if nargin<3, error('Not enough input arguments.'); end
[I,J,T]=size(A);
if L~=2*floor(L/2), error('The length of the demixing filters must be even.'); end
if I*L >= 1024, warning('BSSOracle:IntensiveComputation','Computing %d filter coefficients for %d targets. This may take some time.',I*L,J); end;

%%% Computing coefficients of least squares problem via FFT %%%
% Zero padding and FFT of mixing filters
N=2^nextpow2(T+L-1);
Af=fft(A,N,3);
% Inner products between delayed mixing filters
G=zeros(I*L);
for i1=0:I-1,
    for i2=0:i1,
        Gf=zeros(N,1);
        for j=1:J,
            AAf=reshape(Af(i1+1,j,:).*conj(Af(i2+1,j,:)),N,1);
            Gf=Gf+real(ifft(AAf));
        end
        AA=toeplitz(Gf([1 N:-1:N-L+2]),Gf(1:L));
        G(i1*L+1:i1*L+L,i2*L+1:i2*L+L)=AA;
        G(i2*L+1:i2*L+L,i1*L+1:i1*L+L)=AA.';
    end
end
% Inner products between delayed mixing filters and unitary filters
D=zeros(I*L,J);
for i=0:I-1,
    D(i*L+max(1,zdel+L/2-T):i*L+min(L,zdel+L/2-1),:)=reshape(A(i+1,:,min(T,zdel+L/2-1):-1:max(1,zdel-L/2)),J,min(T,zdel+L/2-1)-max(1,zdel-L/2)+1).';
end

%%% Computing pseudo-inverse demixing filters %%%
W=inv(G)*D;
W=permute(reshape(W,L,I,J),[3 2 1]);

%%% Deriving product of demixing and mixing filters %%%
% Applying pseudo-inverse demixing filters
B=zeros(J,J,T+L-1);
for j=1:J;
    for k=1:J,
        for i=1:I,
            B(j,k,:)=reshape(B(j,k,:),1,T+L-1)+fftfilt(W(j,i,:),[reshape(A(i,k,:),1,T),zeros(1,L-1)]);
        end
    end
end
% Defining target product
Bt=zeros(J,J,T+L-1);
for j=1:J,
    Bt(j,j,zdel+L/2-1)=1;
end
% SIR
SIR=10*log10(J/sum(sum(sum((B-Bt).^2))));

return;
