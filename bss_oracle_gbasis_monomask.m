function [Se,btree,SDR,gSDR,stree]=bss_oracle_gbasis_monomask(x,S,Dmin,Dmax,pcos,mreal,mconst)

% BSS_ORACLE_GBASIS_MONOMASK Oracle estimator for single-channel source
% separation of several mixtures by time-frequency masking using the best 
% generic CP/WP basis.
%
% [Se,btree,SDR,gSDR,stree]=bss_oracle_gbasis_monomask(x,S,Dmin,Dmax)
% [Se,btree,SDR,gSDR,stree]=bss_oracle_gbasis_monomask(x,S,Dmin,Dmax,pcos)
% [Se,btree,SDR,gSDR,stree]=bss_oracle_gbasis_monomask(x,S,Dmin,Dmax,pcos,mreal)
% [Se,btree,SDR,gSDR,stree]=bss_oracle_gbasis_monomask(x,S,Dmin,Dmax,pcos,mreal,mconst)
%
% Warnings:
% Despite the default setting, this function is mostly relevant for WP bases.
% Due to huge memory requirements, the optimal masks are not output.
% The function stores temporary data in a temporary directory defined by the
% variable tmpdir (default is /tmp/).
%
% Inputs:
% x: 1 x T x K table containing K single-channel mixture signals
% S: J x T x K table containing K sets of target signals (e.g. source images)
% Dmin: minimal transform depth
% Dmax: maximal transform depth
% pcos: true for CP with sine window (default)
%       false for WP with symmlet-8
% mreal: true for real-valued masking (default)
%        false for binary masking
% mconst: true when the masks are subject to a unitary sum constraint (default)
%         false otherwise
%
% Outputs:
% Se: J x T x K table containing the oracle estimates of the target signals
%     (truncated to the same time range as the original signals)
% btree: 1 x (2^(Dmax+1)-1) vector of binary values representing the tree
%        structure corresponding to the oracle best generic basis
% SDR: K x 1 vector containing the achieved SDR in deciBels for each
% mixture (before truncation of the target estimates)
% gSDR: total SDR for all mixtures
% stree: 1 x (2^(Dmax+1)-1) vector containing the oracle distortion for all basis
% elements (infinite for disallowed scales)
%
% Reference:
% E. Vincent, R. Gribonval and M.D. Plumbley, "Oracle estimators for the
% benchmarking of source separation algorithms," Technical Report
% C4DM-TR-06-03, Queen Mary, University of London, 2006, Section 7.3.2.
%
% See also CPAnalysis, CPSynthesis, WPAnalysis, WPSynthesis.

%%% Errors and warnings %%%
if nargin<4, error('Not enough input arguments.'); end
if nargin<5, pcos=true; end
if nargin<6, mreal=true; end
if nargin<7, mconst=true; end
[I,T,K]=size(x);
[J,Ts,Ks]=size(S);
if T~=Ts, error('Mixture and target signals have different lengths.'); end
if K~=Ks, error('The numbers of mixture and target signals are not consistent.'); end
if mconst && any(any(abs(x-sum(S,1)) > eps)), error('When computing constrained masks, the mixture signal must be equal to the sum of the target signals.'); end
if (J==2), mconst=false; end
if Dmin>Dmax, error('The minimal depth must be smaller than the maximal depth.'); end
N=nextpow2(T);
if mreal && mconst, warning('BSSOracle:IntensiveComputation','Computing constrained real-valued masks for %d\nbasis elements and %d sources. This may take some time.',(Dmax-Dmin+1)*(2^N)*K,J); end;

%%% Temporary directory %%%
tmpdir='/tmp/';

%%% Computing input data transforms %%%
% Zero-padding
x=cat(2,x,zeros(1,2^N-T,K));
S=cat(2,S,zeros(J,2^N-T,K));
if pcos,
    % CP transform
    for k=1:K,
        X=CPAnalysis(x(1,:,k),Dmax);
        X=X(:,Dmin+1:Dmax+1);
        save([tmpdir 'X' int2str(k)],'X');
        SS=zeros(2^N,Dmax-Dmin+1,J);
        for j=1:J,
            Y=CPAnalysis(S(j,:,k),Dmax);
            SS(:,:,j)=Y(:,Dmin+1:Dmax+1);
        end
        save([tmpdir 'SS' int2str(k)],'SS');
    end
else
    % WP transform
    for k=1:K,
        qmf=MakeONFilter('Symmlet',8);
        X=WPAnalysis(x(1,:,k),Dmax,qmf);
        X=X(:,Dmin+1:Dmax+1);
        save([tmpdir 'X' int2str(k)],'X');
        SS=zeros(2^N,Dmax-Dmin+1,J);
        for j=1:J,
            Y=WPAnalysis(S(j,:,k),Dmax,qmf);
            SS(:,:,j)=Y(:,Dmin+1:Dmax+1);
        end
        save([tmpdir 'SS' int2str(k)],'SS');
    end
end
clear x;

%%% Computing oracle mask and distortion for each element %%%
stree=zeros(1,2^(Dmax+1)-1);
for d=0:Dmax,
    if d < Dmin,
        % Infinite distortion for disallowed scales
        stree(node(d,0:2^d-1))=inf;
    else
        Xd=zeros(2^N,K);
        SSd=zeros(2^N,J,K);
        Wd=zeros(2^N,J,K);
        for k=1:K,
            load([tmpdir 'X' int2str(k)],'X');
            load([tmpdir 'SS' int2str(k)],'SS');
            Xd(:,k)=X(:,d-Dmin+1);
            SSd(:,:,k)=reshape(SS(:,d-Dmin+1,:),2^N,J);
        end
        for b=0:(2^d-1),
            Xp=Xd(packet(d,b,2^N),:);
            Sp=SSd(packet(d,b,2^N),:,:);
            % Ratio of target and mixture coefficients
            Rp=Sp./repmat(permute(Xp+realmin,[1 3 2]),[1 J 1]);
            Wp=zeros(2^(N-d),J,K);
            if mconst && mreal,
                % Constrained real-valued mask
                for t=1:2^(N-d),
                    for k=1:K,
                        if any(Rp(t,:,k)),
                            Wp(t,:,k)=optim_coeffs(Rp(t,:,k).');
                        else
                            Wp(t,:,k)=1/J;
                        end
                    end
                end
            elseif mconst && ~mreal,
                % Constrained binary mask
                for k=1:K,
                    [val,ind]=max(Rp(:,:,k),[],2);
                    Wp(sub2ind([2^(N-d) J K],(1:2^(N-d)).',ind,k*ones(2^(N-d),1)))=1;
                end
            elseif ~mconst && mreal,
                % Unconstrained real-valued mask
                Wp=min(max(Rp,0),1);
            else
                % Unconstrained binary mask
                Wp=(Rp >= .5);
            end
            % Computing distortion and storing oracle masks
            Wd(packet(d,b,2^N),:,:)=Wp;
            stree(node(d,b))=sum(sum(sum((Sp-Wp.*repmat(permute(Xp,[1 3 2]),[1 J 1])).^2)));
        end
        save([tmpdir 'Wd' int2str(d)],'Wd');
    end
end
clear Xd SSd SS;

%%% Selecting best basis %%%
global WLVERBOSE;
WLVERBOSE='No';
btree=BestBasis(stree,Dmax);

%%% Deriving oracle estimates of the target signals %%%
Se=zeros(J,2^N,K);
SDR=zeros(K,1);
for k=1:K,
    load([tmpdir 'X' int2str(k)],'X');
    W=zeros(2^N,Dmax-Dmin+1,J);
    for d=Dmin:Dmax,
        load([tmpdir 'Wd' int2str(d)],'Wd');
        W(:,d-Dmin+1,:)=Wd(:,:,k);
    end
    for j=1:J,
        % Applying oracle mask
        Y=[zeros(2^N,Dmin),X.*W(:,:,j)];
        if pcos,
            % Inverse CP transform (using best basis only)
            Se(j,:,k)=CPSynthesis(btree,Y);
        else
            % Inverse WP transform (using best basis only)
            Se(j,:,k)=WPSynthesis(btree,Y,qmf);
        end
    end
    % SDR
    SDR(k)=sdr(Se(:,:,k),S(:,:,k));
end
clear X W Wd;
% gSDR
gSDR=sdr(reshape(Se,J*T,K),reshape(S,J*T,K));
% Truncating target signal estimates to original target length
Se=Se(:,1:T,:);

return;
